package pl.gdziejaide.gjiandroid.api;

import pl.gdziejaide.gjiandroid.enums.ObjectType;
import pl.gdziejaide.gjiandroid.gui.Dialogs;
import pl.gdziejaide.gjiandroid.gui.MusicPlayer;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.Media;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import android.content.Context;
import android.util.Log;

/**
 * Klasa implementuj�ca GUI API
 * 
 * @author Przemek
 * 
 */
public class GUI {
	public static Context context;
	public static Context getContext() {
		return context;
	}
	public static void setContext(Context context) {
		GUI.context = context;
	}
	public static void message(String title, String message) {
		Dialogs dialogs = new Dialogs();
		dialogs.showMessage(title, message);
	}
	public static void warning(String title, String message) {
		Dialogs dialogs = new Dialogs();
		dialogs.showWarning(title, message);
	}
	public static int choice(String[] params) {
		Dialogs dialogs = new Dialogs();
		return dialogs.showChoice(params);
	}
	// public static int[] multipleChoice(String[] params) {
	// return new int[0];
	// }
	public static String dialog(String title, String message) {
		Dialogs dialogs = new Dialogs();
		return dialogs.showDialog(title, message);
	}
	public static boolean confirm(String title, String message) {
		Dialogs dialogs = new Dialogs();
		return dialogs.showConfirm(title, message);
	}
	public static boolean showMedia(int mediaID) {
		GJIObject media = ObjectsContainer.getById(mediaID);
		if (media.getObjectType() == ObjectType.IMAGE) {
			media.event("onshow");
		} else {
			Log.d("muza", "Jestem w GUI.java!");
			MusicPlayer.play((Media)media);
			media.event("onplay");
		}
		// kod obsługi mediów
		return false;
	}
}
