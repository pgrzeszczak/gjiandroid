package pl.gdziejaide.gjiandroid.api;

/**
 * Klasa realizująca MATH API
 * 
 * @author Przemek
 *
 */
public class MATH {
	public static int sum(int[] a)
	{
		int sum = 0;
		for(int i : a) {
			sum += i;
		}
		return sum;
	}
	public static double sum(double[] a)
	{
		double sum = 0;
		for(double i : a) {
			sum += i;
		}
		return sum;
	}
	public static int add(int a, int b)
	{
		return a + b;
	}
	public static double add(double a, double b)
	{
		return a + b;
	}
	public static int minus(int a, int b)
	{
		return a - b;
	}
	public static double minus(double a, double b)
	{
		return a - b;
	}
	public static int abs(int a)
	{
		return a > 0 ? a : -a;
	}
	public static double abs(double a)
	{
		return a > 0 ? a : -a;
	}
	public static int div(int a, int b)
	{
		return a / b;
	}
	public static double div(double a, double b)
	{
		return a / b;
	}
	public static int mod(int a, int b)
	{
		return a % b;
	}
	public static int multi(int a, int b)
	{
		return a * b;
	}
	public static double multi(double a, double b)
	{
		return a * b;
	}
	public static int power(int a, int b)
	{
		int result = 1;
		while(b > 0)
		{
			result *= a;
			b--;
		}
		return result;
	}
	public static double power(double a, double b)
	{
		return (double) Math.pow(a, b);
	}
	public static double root(double a, double b)
	{
		return (double) Math.pow(a, 1.0 / b);
	}
	public static int max(int a, int b)
	{
		return a > b ? a : b;
	}
	public static double max(double a, double b)
	{
		return a > b ? a : b;
	}
	public static int min(int a, int b)
	{
		return a < b ? a : b;
	}
	public static double min(double a, double b)
	{
		return a < b ? a : b;
	}
	public static int round(double a)
	{
		return (int) Math.round(a);
	}
}
