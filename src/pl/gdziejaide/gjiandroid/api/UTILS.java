package pl.gdziejaide.gjiandroid.api;

import pl.gdziejaide.gjiandroid.enums.CharacterState;
import pl.gdziejaide.gjiandroid.enums.ItemState;
import pl.gdziejaide.gjiandroid.enums.QuestState;
import pl.gdziejaide.gjiandroid.enums.ZoneState;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Timer;

/**
 * Klasa implementująca UTILS API
 *
 * @author Przemek
 *
 */
public class UTILS {
	public static Object set(Object value) {
		return value;
	}
	public static int stringToInt(String value) {
		int parsed = 0;
		try {
			parsed = Integer.parseInt(value);
		} catch (Exception e) {
		}
		return parsed;
	}
	public static double stringToDouble(String value) {
		double parsed = 0;
		try {
			parsed = Double.parseDouble(value);
		} catch (Exception e) {
		}
		return parsed;
	}
	public static void changeZoneState(int zoneId, ZoneState zoneState)
	{
		ObjectsContainer.getById(zoneId).set("state", zoneState);
	}
	public static void changeCharacterState(int characterId, CharacterState characterState)
	{
		ObjectsContainer.getById(characterId).set("state", characterState);
	}
	public static void changeItemState(int itemId, ItemState itemState)
	{
		ObjectsContainer.getById(itemId).set("state", itemState);
	}
	public static void changeQuestState(int questId, QuestState questState)
	{
		ObjectsContainer.getById(questId).set("state", questState);
	}
	public static void changeZoneVisibility(int zoneId, boolean visible)
	{
		ObjectsContainer.getById(zoneId).set("visible", visible);
	}
	public static void changeCharacterVisibility(int characterId, boolean visible)
	{
		ObjectsContainer.getById(characterId).set("visible", visible);
	}
	public static void changeItemVisibility(int itemId, boolean visible)
	{
		ObjectsContainer.getById(itemId).set("visible", visible);
	}
	public static void changeQuestVisibility(int questId, boolean visible)
	{
		ObjectsContainer.getById(questId).set("visible", visible);
	}
	public static void changeCharacterZone(int characterId, int zoneId)
	{
		ObjectsContainer.getById(characterId).set("zone", zoneId);
	}
	public static void startTimer(int timerId)
	{
		Timer timer = (Timer)ObjectsContainer.getById(timerId);
		timer.start();
	}
	public static void stopTimer(int timerId)
	{
		Timer timer = (Timer)ObjectsContainer.getById(timerId);
		timer.stop();
	}
}
