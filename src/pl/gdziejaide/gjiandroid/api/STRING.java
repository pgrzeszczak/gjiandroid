package pl.gdziejaide.gjiandroid.api;

/**
 * Klasa realizująca STRING API
 * 
 * @author Przemek
 *
 */
public class STRING {
	public static String append(String a, String b)
	{
		return a + b;
	}
	public static boolean contains(String a, String b)
	{
		return a.contains(b);
	}
	public static String remove(String a, String b)
	{
		return a.replace(b, "");
	}
}
