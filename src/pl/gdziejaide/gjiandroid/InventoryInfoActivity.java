package pl.gdziejaide.gjiandroid;

import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class InventoryInfoActivity extends MyActivity {

	int itemId;
	Item item;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_inventory_info);
		setDimensions();

		Intent intent = getIntent();
		itemId = intent.getIntExtra("ITEM_ID", -1);
		item = (Item) ObjectsContainer.getById(itemId);

		completeInfo();
	}

	public void completeInfo() {
		TextView tv = (TextView) findViewById(R.id.textView1);
		tv.setText(item.name);
		tv = (TextView) findViewById(R.id.textView2);
		tv.setText(item.description);
	}
	public void finishHim(View view) {
		Intent intent = new Intent(this, InventoryListActivity.class);
		startActivity(intent);
		finish();
	}

	public void onBackPressed() {
		Intent intent = new Intent(this, InventoryListActivity.class);
		startActivity(intent);
		finish();
	}

	public void takeItem(View view) {
	}

}
