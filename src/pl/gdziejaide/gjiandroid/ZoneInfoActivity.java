package pl.gdziejaide.gjiandroid;

import java.io.File;

import pl.gdziejaide.gjiandroid.map.MyMapView;
import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.Media;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Zone;
import pl.gdziejaide.gjiandroid.utils.Point;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;

public class ZoneInfoActivity extends MyActivity {

	private int zoneId;
	int xml_id;
	private Zone zone;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_zone_info);
		setDimensions();

		Intent intent = getIntent();
		zoneId = intent.getIntExtra("ZONE_ID", -1);
		zone = (Zone) ObjectsContainer.getById(zoneId);

		xml_id=ObjectsContainer.getXmlId();
		
		if (zone.icon != -1) {			
			Media image = ObjectsContainer.getImage(zone.icon);
			ImageView iv = (ImageView) findViewById(R.id.imageView1);
			File SDCardRoot = Environment.getExternalStorageDirectory();
			String bmPath = SDCardRoot.getAbsolutePath() + "/gji/scenario/scenario" + xml_id + "/" + image.getPath();
			Bitmap bm = BitmapFactory.decodeFile(bmPath);
			iv.setImageBitmap(bm);
		}
		
		completeInfo();
	}

	public void completeInfo() {
		TextView tv = (TextView) findViewById(R.id.textView1);
		tv.setText(zone.name);
		tv = (TextView) findViewById(R.id.textView2);
		tv.setText(zone.description);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home :
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void finishHim(View view) {
		Intent intent = new Intent(this, ZoneListActivity.class);
		startActivity(intent);
		finish();
	}

	public void animateToZone(View view) {
		int lt = 0, ln = 0;
		for (Point p : zone.getPoints()) {
			lt += p.getLatitude();
			ln += p.getLongitude();
		}
		lt /= zone.getPoints().size();
		ln /= zone.getPoints().size();
		GeoPoint p = new GeoPoint(lt, ln);
		MyMapView.mapa.animateToPoint(p);
		finish();
	}

	public void onBackPressed() {
		Intent intent = new Intent(this, ZoneListActivity.class);
		startActivity(intent);
		finish();
	}
}
