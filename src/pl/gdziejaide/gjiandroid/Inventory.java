package pl.gdziejaide.gjiandroid;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
	public static Inventory inventory;
	public List<Integer> items = new ArrayList<Integer>();
	public void add(int id) {
		items.add(id);
	}
	public void remove(int id) {
		items.remove(items.indexOf(id));
	}
	public List<Integer> list() {
		return items;
	}
	public Inventory() {
		Inventory.inventory=this;
	}
}
