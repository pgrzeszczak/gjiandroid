package pl.gdziejaide.gjiandroid.gui;

import pl.gdziejaide.gjiandroid.R;
import pl.gdziejaide.gjiandroid.api.GUI;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Dialogs {
	Context mContext;
	public int result = -1;
	public String resultText = "";
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public Dialogs() {
		mContext = GUI.getContext();
	}
	public void showMessage(final String title, final String message) {
		result = -1;
		final Activity activity = (Activity) mContext;
		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
				dialog.setTitle(title);
				dialog.setMessage(message);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						result = 0;
					}
				});
				dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						result = 0;
					}
				});
				dialog.show();
			}
		});
		while (result == -1) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	public void showWarning(final String title, final String message) {
		result = -1;
		final Activity activity = (Activity) mContext;
		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
				dialog.setTitle(title);
				dialog.setMessage(message);
				dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						result = 0;
					}
				});
				dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						result = 0;
					}
				});
				dialog.show();
			}
		});
		while (result == -1) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public int showChoice(String[] params) {
		result = -1;
		String[] choices = new String[params.length - 1];
		System.arraycopy(params, 1, choices, 0, params.length - 1);

		final String title = params[0];
		final String[] choices2 = choices;
		final Activity activity = (Activity) mContext;

		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Dialog dia = new Dialog(mContext);
				LayoutInflater inflater = LayoutInflater.from(dia.getContext());
				dia.setTitle(title);
				dia.setCancelable(false);
				dia.setContentView(inflater.inflate(R.layout.dialog_choice, null));

				ListView lv = (ListView) dia.findViewById(R.id.listView1);
				lv.setAdapter(new ArrayAdapter<String>(activity.getApplicationContext(), android.R.layout.simple_list_item_1, choices2));
				lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
				lv.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
						int which = arg2;
						result = which;
						Toast.makeText(mContext, choices2[which], Toast.LENGTH_SHORT).show();
						dia.dismiss();
					}
				});

				dia.show();
			}
		});

		while (result == -1) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	public String showDialog(final String title, final String message) {
		result = -1;
		resultText = "";
		final Activity activity = (Activity) mContext;

		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Dialog dia = new Dialog(mContext);
				LayoutInflater inflater = LayoutInflater.from(dia.getContext());
				dia.setTitle(title);
				dia.setCancelable(false);
				dia.setContentView(inflater.inflate(R.layout.dialog_dialog, null));

				WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
				lp.copyFrom(dia.getWindow().getAttributes());
				lp.width = WindowManager.LayoutParams.FILL_PARENT;
				lp.height = WindowManager.LayoutParams.FILL_PARENT;
				dia.getWindow().setAttributes(lp);

				final TextView tv = (TextView) dia.findViewById(R.id.textView1);
				tv.setText(message);

				final EditText et = (EditText) dia.findViewById(R.id.editText1);

				Button bt = (Button) dia.findViewById(R.id.button1);
				bt.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						resultText = et.getText().toString();
						result = 1;
						dia.cancel();
					}
				});

				dia.show();
			}
		});

		while (result == -1) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return resultText;
	}
	public boolean showConfirm(final String title, final String message) {
		result = -1;

		final Activity activity = (Activity) mContext;

		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
				dialog.setTitle(title);
				dialog.setMessage(message);
				dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						result = 0;
					}
				});
				dialog.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						result = 1;
					}
				});
				dialog.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						result = 0;
					}
				});
				dialog.show();
			}
		});

		while (result == -1) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (result == 0)
			return false;
		else
			return true;
	}
}
