package pl.gdziejaide.gjiandroid.gui;

import java.io.File;
import java.io.IOException;

import pl.gdziejaide.gjiandroid.api.GUI;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Environment;
import android.util.Log;

public class MusicPlayer {
	public static MediaPlayer mp=new MediaPlayer();
	public static void play(pl.gdziejaide.gjiandroid.objects.Media media) {
		if(!mp.isPlaying()) {
			File SDCardRoot = Environment.getExternalStorageDirectory();
			String path = SDCardRoot.getAbsolutePath() + "/gji/scenario/scenario" + ObjectsContainer.getXmlId() + "/assets/" + media.getPath();
			try {
				mp=new MediaPlayer();
				Log.d("muza", "Przygotowuję muzę..!");
				mp.setDataSource(path);
				Log.d("muza", "Ustawiłem path..! "+path);
				mp.prepare();
				Log.d("muza", "Przygotowałem muzę..!");
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Log.d("muza", "Puszczam muzę..!");
						mp.start();
					}
				}).start();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	public static void stop() {
		if(mp.isPlaying()) mp.stop();
		
	}
}
