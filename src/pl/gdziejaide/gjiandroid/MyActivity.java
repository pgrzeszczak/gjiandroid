package pl.gdziejaide.gjiandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class MyActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}
	
	@SuppressWarnings("deprecation")
	public void setDimensions() {
		WindowManager.LayoutParams params = getWindow().getAttributes();
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		switch (metrics.densityDpi) {
			case DisplayMetrics.DENSITY_HIGH : {
				height -= 48;
				break;
			}
			case DisplayMetrics.DENSITY_MEDIUM : {
				height -= 32;
				break;
			}
			case DisplayMetrics.DENSITY_LOW : {
				height -= 24;
				break;
			}
		}

		params.width = (int) (0.95 * width);
		params.height = (int) (0.95 * height);
		this.getWindow().setAttributes(params);
	}

}
