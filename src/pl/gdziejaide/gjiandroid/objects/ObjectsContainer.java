package pl.gdziejaide.gjiandroid.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.gdziejaide.gjiandroid.enums.ObjectType;
import android.annotation.SuppressLint;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.util.Log;

/**
 * Klasa statyczna przechowująca wszystkie obiekty
 * 
 * @author Przemek
 * 
 */
public class ObjectsContainer {
	@SuppressLint("UseSparseArrays")
	static Map<Integer, GJIObject> list = new TreeMap<Integer, GJIObject>();
	static List<Zone> zones = new ArrayList<Zone>();
	static List<Character> characters = new ArrayList<Character>();
	static List<Item> items = new ArrayList<Item>();
	static List<Quest> quests = new ArrayList<Quest>();
	static List<Media> images = new ArrayList<Media>();
	static Scenario scenario = null;
	static int xml_id;

	public static void addObject(int id, GJIObject object) {
		list.put(id, object);
		if (object.getObjectType() == ObjectType.ZONE) {
			zones.add((Zone) object);
		} else if (object.getObjectType() == ObjectType.CHARACTER) {
			characters.add((Character) object);
		} else if (object.getObjectType() == ObjectType.ITEM) {
			items.add((Item) object);
		} else if (object.getObjectType() == ObjectType.QUEST) {
			quests.add((Quest) object);
		} else if (object.getObjectType() == ObjectType.SCENARIO) {
			scenario = (Scenario) object;
		} else if (object.getObjectType() == ObjectType.IMAGE) {
			images.add((Media) object);
		}
	}

	public static void addObjects(Map<Integer, GJIObject> objects) {
		list.putAll(objects);
		for (GJIObject object : objects.values()) {
			if (object.getObjectType() == ObjectType.ZONE) {
				zones.add((Zone) object);
			} else if (object.getObjectType() == ObjectType.CHARACTER) {
				characters.add((Character) object);
			} else if (object.getObjectType() == ObjectType.ITEM) {
				items.add((Item) object);
			} else if (object.getObjectType() == ObjectType.QUEST) {
				quests.add((Quest) object);
			} else if (object.getObjectType() == ObjectType.IMAGE) {
				images.add((Media) object);
			} else if (object.getObjectType() == ObjectType.SCENARIO) {
				scenario = (Scenario) object;
			}
		}
	}

	public static GJIObject getById(int id) {
		return list.get(id);
	}

	public static Map<Integer, GJIObject> getAll() {
		return list;
	}

	public static List<Zone> getZones() {
		return zones;
	}
	public static List<Item> getItems() {
		return items;
	}
	public static List<Quest> getQuests() {
		return quests;
	}
	public static List<Media> getImages() {
		return images;
	}
	public static Media getImage(int id) {
		for(int i=0; i<images.size(); i++) {
			if(images.get(i).id==id) return images.get(i);
		}
		return null;
	}
	public static List<Character> getCharacters() {
		return characters;
	}

	public static List<Timer> getTimers() {
		List<Timer> timers = new ArrayList<Timer>();
		for (GJIObject object : list.values()) {
			if (object.getObjectType() == ObjectType.TIMER) {
				timers.add((Timer) object);
			}
		}
		return timers;
	}

	public static Scenario getScenario() {
		return scenario;
	}

	public static int getXmlId() {
		//return xml_id;
		return 2;
	}
	
	public static void clearAll() {
		list.clear();
		zones.clear();
		items.clear();
		quests.clear();
		images.clear();
		characters.clear();
		xml_id=-1;
		scenario = null;
	}

	public static void setXmlId(int i) {
		xml_id=i;
		
	}
}
