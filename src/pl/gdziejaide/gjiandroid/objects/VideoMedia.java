package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public class VideoMedia extends Media {

	public VideoMedia(int id, String idname, boolean owned, String path, int zone) {
		super(id, idname, owned, ObjectType.VIDEO, path, zone);
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.PLAY);
	}
}
