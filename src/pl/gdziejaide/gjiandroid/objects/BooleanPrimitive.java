package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public class BooleanPrimitive extends Primitive {
	public boolean value;

	public BooleanPrimitive(int id, String idname, boolean owned, boolean value) {
		super(id, idname, ObjectType.BOOLEAN, owned);
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
		event("onvaluechange");
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("value")) {
			return getValue();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("value")) {
			setValue((Boolean)value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("value")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
}
