package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public abstract class Media extends GJIObject {
	public String path;
	public int zone;

	public Media(int id, String idname, boolean owned, ObjectType objectType, String path, int zone) {
		super(id, idname, objectType, owned);
		this.path = path;
		this.zone = zone;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public int getZone() {
		return zone;
	}

	public void setZone(int zone) {
		this.zone = zone;
	}

	public Object get(String name) {
		if (name.equalsIgnoreCase("path")) {
			return getPath();
		} else if (name.equalsIgnoreCase("zone")) {
			return getZone();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("path")) {
			setPath((String) value);
		} else if (name.equalsIgnoreCase("zone")) {
			setZone((Integer) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("path")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("zone")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
}
