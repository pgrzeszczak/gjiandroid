package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ItemState;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public class Item extends GJIObject {
	public String name;
	public String description;
	public int zone;
	public boolean visible;
	public ItemState state;
	public int icon;

	public Item(int id, String idname, boolean owned, String name, String description, int zone,
			boolean visible, ItemState state, int icon) {
		super(id, idname, ObjectType.ITEM, owned);
		this.name = name;
		this.description = description;
		this.zone = zone;
		this.visible = visible;
		this.state = state;
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getZone() {
		return zone;
	}

	public void setZone(int zone) {
		this.zone = zone;
		event("onzonechange");
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		event("onvisiblechange");
	}

	public ItemState getState() {
		return state;
	}

	public void setState(ItemState state) {
		this.state = state;
		event("onstatechange");
	}
	
	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("name")) {
			return getName();
		} else if (name.equalsIgnoreCase("description")) {
			return getDescription();
		} else if (name.equalsIgnoreCase("zone")) {
			return getZone();
		} else if (name.equalsIgnoreCase("visible")) {
			return isVisible();
		} else if (name.equalsIgnoreCase("state")) {
			return getState();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("icon")) {
			return getIcon();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("name")) {
			setName((String) value);
		} else if (name.equalsIgnoreCase("description")) {
			setDescription((String) value);
		} else if (name.equalsIgnoreCase("zone")) {
			setZone((Integer) value);
		} else if (name.equalsIgnoreCase("visible")) {
			setVisible((Boolean) value);
		} else if (name.equalsIgnoreCase("state")) {
			setState((ItemState) value);
		} else if (name.equalsIgnoreCase("icon")) {
			setIcon((Integer) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("name")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("description")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("zone")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("visible")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("state")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("icon")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.VISIBLE_CHANGE);
		availableEvents.add(EventType.STATE_CHANGE);
		availableEvents.add(EventType.ZONE_CHANGE);
		availableEvents.add(EventType.FIND);
		availableEvents.add(EventType.DROP);
		availableEvents.add(EventType.PICK);
	}
}
