package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public class ImageMedia extends Media {

	public ImageMedia(int id, String idname, boolean owned, String path, int zone) {
		super(id, idname, owned, ObjectType.IMAGE, path, zone);
	}

	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.SHOW);
	}
}
