package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public class CharPrimitive extends Primitive {
	public char value;

	public CharPrimitive(int id, String idname, boolean owned, char value) {
		super(id, idname, ObjectType.CHAR, owned);
		this.value = value;
	}

	public char getValue() {
		return value;
	}

	public void setValue(char value) {
		this.value = value;
		event("onvaluechange");
	}

	public Object get(String name) {
		if (name.equalsIgnoreCase("value")) {
			return getValue();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("value")) {
			setValue((java.lang.Character) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("value")) {
			return ArgumentType.CHAR;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
}
