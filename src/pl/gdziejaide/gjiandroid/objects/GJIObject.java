package pl.gdziejaide.gjiandroid.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

/**
 * Klasa nadrzędna dla wszystkich obiektów (zone, scenario, ...)
 *
 * @author Przemek
 *
 */
public abstract class GJIObject {
	protected int id;
	protected String idname;
	protected boolean owned;
	protected ObjectType objectType;
	protected Map<EventType, EventAction> registeredEvents;
	protected List<EventType> availableEvents;
	
	public GJIObject(int id, String idname, ObjectType objectType, boolean owned)
	{
		this.id = id;
		this.idname = idname;
		this.objectType = objectType;
		this.owned = owned;
		registeredEvents = new TreeMap<EventType, EventAction>();
		availableEvents = new ArrayList<EventType>();
		prepareAvailableEvents();
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getIdname() {
		return idname;
	}
	
	public boolean isOwned() {
		return owned;
	}
	
	public void setOwned(boolean owned) {
		this.owned = owned;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void registerEvent(EventAction event) {
		registeredEvents.put(event.getType(), event);
	}
	
	public void event(String eventName, boolean quiet)
	{
		EventType eventType = EventType.getFromString(eventName);
		if (availableEvents.contains(eventType)) {
			EventAction event = registeredEvents.get(eventType);
			if (event != null) {
				event.process();
			} else if (!quiet) {
				System.err.println("Próba wywołania niezarejstrowanego eventu \"" + eventName + "\" na obiekcie o id " + id);
			}
		} else {
			System.err.println("Próba wywołania niestniejącego eventu \"" + eventName + "\" na obiekcie o id " + id);
		}
	}
	
	public void event(String eventName)
	{
		event(eventName, true);
	}
	
	public abstract Object get(String name);
	public abstract void set(String name, Object value);
	protected abstract void prepareAvailableEvents();
	public abstract ArgumentType getFieldType(String name);
}
