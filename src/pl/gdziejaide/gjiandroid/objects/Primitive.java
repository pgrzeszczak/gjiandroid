package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public abstract class Primitive extends GJIObject {

	public Primitive(int id, String idname, ObjectType objectType, boolean owned) {
		super(id, idname, objectType, owned);
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.VALUE_CHANGE);
	}
}
