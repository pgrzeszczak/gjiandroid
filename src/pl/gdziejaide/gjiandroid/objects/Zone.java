package pl.gdziejaide.gjiandroid.objects;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;
import pl.gdziejaide.gjiandroid.enums.ZoneState;
import pl.gdziejaide.gjiandroid.utils.Point;

public class Zone extends GJIObject {
	public String name;
	public String description;
	public int radius;
	public ZoneState state;
	public boolean visible;
	public List<Point> points;
	public int icon;

	public Zone(int id, String idname, boolean owned, String name, String description, int radius,
			ZoneState state, boolean visible, int icon, List<Point> points) {
		super(id, idname, ObjectType.ZONE, owned);
		this.name = name;
		this.description = description;
		this.radius = radius;
		this.state = state;
		this.visible = visible;
		this.points = points;
		this.icon = icon;
	}
	
	public Zone(int id, String idname, boolean owned, String name, String description, int radius,
			ZoneState state, boolean visible, int icon) {
		this(id, idname, owned, name, description, radius,	state, visible, icon, new ArrayList<Point>());
	}
	
	public void addPoint(Point point)
	{
		points.add(point);
	}
	
	public void clearPoints()
	{
		points.clear();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public ZoneState getState() {
		return state;
	}

	public void setState(ZoneState state) {
		this.state = state;
		event("onstatechange");
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		event("onvisiblechange");
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("name")) {
			return getName();
		} else if (name.equalsIgnoreCase("description")) {
			return getDescription();
		} else if (name.equalsIgnoreCase("radius")) {
			return getRadius();
		} else if (name.equalsIgnoreCase("state")) {
			return getState();
		} else if (name.equalsIgnoreCase("visible")) {
			return isVisible();
		} else if (name.equalsIgnoreCase("points")) {
			return getPoints();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("icon")) {
			return getIcon();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("name")) {
			setName((String) value);
		} else if (name.equalsIgnoreCase("description")) {
			setDescription((String) value);
		} else if (name.equalsIgnoreCase("radius")) {
			setRadius((Integer) value);
		} else if (name.equalsIgnoreCase("state")) {
			setState((ZoneState) value);
		} else if (name.equalsIgnoreCase("visible")) {
			setVisible((Boolean) value);
		} else if (name.equalsIgnoreCase("points")) {
			setPoints((List<Point>) value);
		} else if (name.equalsIgnoreCase("icon")) {
			setIcon((Integer) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("name")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("description")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("radius")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("state")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("visible")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("points")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("icon")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.ENTER);
		availableEvents.add(EventType.LEAVE);
		availableEvents.add(EventType.GET_CLOSER);
		availableEvents.add(EventType.VISIBLE_CHANGE);
		availableEvents.add(EventType.STATE_CHANGE);
	}
}
