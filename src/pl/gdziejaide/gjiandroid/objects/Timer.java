package pl.gdziejaide.gjiandroid.objects;

import java.util.TimerTask;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

/**
 * TODO
 * @author Przemek
 *
 */
public class Timer extends GJIObject {
	public int interval;
	private java.util.Timer timer = null;
	private boolean running = false;

	public Timer(int id, String idname, boolean owned, int interval) {
		super(id, idname, ObjectType.TIMER, owned);
		this.interval = interval;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		stop();
		this.interval = interval;
		start();
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("interval")) {
			return getInterval();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("interval")) {
			setInterval((Integer) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("interval")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.START);
		availableEvents.add(EventType.STOP);
		availableEvents.add(EventType.TIMER);
	}
	
	public void start() {
		if (timer == null) {
			timer = new java.util.Timer();
			TimerTask task = new TimerTask() {
				
				@Override
				public void run() {
					event("ontimer");
				}
			};
			timer.schedule(task, interval*1000, interval*1000);
			running = true;
			event("onstart");
		}
	}
	
	public void stop() {
		if (timer != null) {
			timer.cancel();
			timer.purge();
			timer = null;
			running = false;
			event("onstop");
		}
	}
	
	public boolean isRunning() {
		return running;
	}
}
