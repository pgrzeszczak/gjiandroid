package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.CharacterState;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;
import pl.gdziejaide.gjiandroid.enums.SexType;

public class Character extends GJIObject {
	public String name;
	public SexType sex;
	public String description;
	public int zone;
	public CharacterState state;
	public boolean visible;
	public int icon;

	public Character(int id, String idname, boolean owned, String name, SexType sex, String description,
			int zone, CharacterState state, boolean visible, int icon) {
		super(id, idname, ObjectType.CHARACTER, owned);
		this.name = name;
		this.sex = sex;
		this.description = description;
		this.zone = zone;
		this.state = state;
		this.visible = visible;
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SexType getSex() {
		return sex;
	}

	public void setSex(SexType sex) {
		this.sex = sex;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getZone() {
		return zone;
	}

	public void setZone(int zone) {
		this.zone = zone;
		event("onzonechange");
	}

	public CharacterState getState() {
		return state;
	}

	public void setState(CharacterState state) {
		this.state = state;
		event("onstateechange");
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		event("onvisiblechange");
	}
	
	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("name")) {
			return getName();
		} else if (name.equalsIgnoreCase("sex")) {
			return getSex();
		} else if (name.equalsIgnoreCase("description")) {
			return getDescription();
		} else if (name.equalsIgnoreCase("zone")) {
			return getZone();
		} else if (name.equalsIgnoreCase("state")) {
			return getState();
		} else if (name.equalsIgnoreCase("visible")) {
			return isVisible();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("icon")) {
			return getIcon();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("name")) {
			setName((String) value);
		} else if (name.equalsIgnoreCase("sex")) {
			setSex((SexType) value);
		} else if (name.equalsIgnoreCase("description")) {
			setDescription((String) value);
		} else if (name.equalsIgnoreCase("zone")) {
			setZone((Integer) value);
		} else if (name.equalsIgnoreCase("state")) {
			setState((CharacterState) value);
		} else if (name.equalsIgnoreCase("visible")) {
			setVisible((Boolean) value);
		} else if (name.equalsIgnoreCase("icon")) {
			setIcon((Integer) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("name")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("sex")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("description")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("zone")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("state")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("visible")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("icon")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.GOOD_MORNING);
		availableEvents.add(EventType.GOOD_BYE);
		availableEvents.add(EventType.VISIBLE_CHANGE);
		availableEvents.add(EventType.STATE_CHANGE);
		availableEvents.add(EventType.ZONE_CHANGE);
		availableEvents.add(EventType.DIALOG);
	}
}
