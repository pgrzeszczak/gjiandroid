package pl.gdziejaide.gjiandroid.objects;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;
import pl.gdziejaide.gjiandroid.enums.QuestState;

public class Quest extends GJIObject {
	public String name;
	public String description;
	public QuestState state;
	public int icon;
	public boolean visible;

	public Quest(int id, String idname, boolean owned, String name, String description, QuestState state, int icon, boolean visible) {
		super(id, idname, ObjectType.QUEST, owned);
		this.name = name;
		this.description = description;
		this.state = state;
		this.icon = icon;
		this.visible = visible;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public QuestState getState() {
		return state;
	}

	public void setState(QuestState state) {
		this.state = state;
		event("onstatechange");
	}
	
	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}
	
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		event("onvisiblechange");
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("name")) {
			return getName();
		} else if (name.equalsIgnoreCase("description")) {
			return getDescription();
		} else if (name.equalsIgnoreCase("state")) {
			return getState();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("icon")) {
			return getIcon();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		} else if (name.equalsIgnoreCase("visible")) {
			return isVisible();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("name")) {
			setName((String) value);
		} else if (name.equalsIgnoreCase("description")) {
			setDescription((String) value);
		} else if (name.equalsIgnoreCase("state")) {
			setState((QuestState) value);
		} else if (name.equalsIgnoreCase("icon")) {
			setIcon((Integer) value);
		} else if (name.equalsIgnoreCase("visible")) {
			setVisible((Boolean) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("name")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("description")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("state")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("icon")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("visible")) {
			return ArgumentType.BOOLEAN;
		}
		return ArgumentType.UNKNOWN;
	}
	
	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.COMPLETE);
		availableEvents.add(EventType.BEGIN);
		availableEvents.add(EventType.INTERACTION);
		availableEvents.add(EventType.STATE_CHANGE);
		availableEvents.add(EventType.VISIBLE_CHANGE);
	}
}
