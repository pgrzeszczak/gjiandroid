package pl.gdziejaide.gjiandroid.objects;

import java.util.Date;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.EventType;
import pl.gdziejaide.gjiandroid.enums.ObjectType;

public class Scenario extends GJIObject {
	public String name;
	public String description;
	public String author;
	public String creatorVersion;
	public String version;
	public Date created;
	
	public Scenario(int id, String idname, boolean owned, String name, String description, String author,
			String creatorVersion, String version, Date created) {
		super(id, idname, ObjectType.SCENARIO, owned);
		this.name = name;
		this.description = description;
		this.author = author;
		this.creatorVersion = creatorVersion;
		this.version = version;
		this.created = created;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCreatorVersion() {
		return creatorVersion;
	}
	public void setCreatorVersion(String creatorVersion) {
		this.creatorVersion = creatorVersion;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Object get(String name) {
		if (name.equalsIgnoreCase("name")) {
			return getName();
		} else if (name.equalsIgnoreCase("description")) {
			return getDescription();
		} else if (name.equalsIgnoreCase("author")) {
			return getAuthor();
		} else if (name.equalsIgnoreCase("creatorversion")) {
			return getCreatorVersion();
		} else if (name.equalsIgnoreCase("version")) {
			return getVersion();
		} else if (name.equalsIgnoreCase("created")) {
			return getCreated();
		} else if (name.equalsIgnoreCase("owned")) {
			return isOwned();
		} else if (name.equalsIgnoreCase("id")) {
			return getId();
		} else if (name.equalsIgnoreCase("idname")) {
			return getIdname();
		}
		return null;
	}
	
	public void set(String name, Object value) {
		if (name.equalsIgnoreCase("name")) {
			setName((String) value);
		} else if (name.equalsIgnoreCase("description")) {
			setDescription((String) value);
		} else if (name.equalsIgnoreCase("author")) {
			setAuthor((String) value);
		} else if (name.equalsIgnoreCase("creatorversion")) {
			setCreatorVersion((String) value);
		} else if (name.equalsIgnoreCase("version")) {
			setVersion((String) value);
		} else if (name.equalsIgnoreCase("created")) {
			setCreated((Date) value);
		}
	}
	
	public ArgumentType getFieldType(String name) {
		if (name.equalsIgnoreCase("name")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("description")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("author")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("creatorversion")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("version")) {
			return ArgumentType.STRING;
		} else if (name.equalsIgnoreCase("created")) {
			return ArgumentType.UNKNOWN;
		} else if (name.equalsIgnoreCase("owned")) {
			return ArgumentType.BOOLEAN;
		} else if (name.equalsIgnoreCase("id")) {
			return ArgumentType.INTEGER;
		} else if (name.equalsIgnoreCase("iname")) {
			return ArgumentType.STRING;
		}
		return ArgumentType.UNKNOWN;
	}
	

	protected void prepareAvailableEvents() {
		availableEvents.add(EventType.BEGIN);
		availableEvents.add(EventType.END);
		availableEvents.add(EventType.RESTORE);
		availableEvents.add(EventType.SAVE);
	}
}
