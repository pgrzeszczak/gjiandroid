package pl.gdziejaide.gjiandroid.utils;

import com.google.android.maps.GeoPoint;

public class Point {
	protected int latitude;
	protected int longitude;

	public Point(int latitude, int longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public int getLatitude() {
		return latitude;
	}
	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
	public int getLongitude() {
		return longitude;
	}
	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}
	public GeoPoint getGeo()
	{
		return new GeoPoint(latitude, longitude);
	}
}
