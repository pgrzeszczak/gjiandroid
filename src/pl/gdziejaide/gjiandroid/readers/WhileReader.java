package pl.gdziejaide.gjiandroid.readers;

import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.Action;
import pl.gdziejaide.gjiandroid.actions.ConditionInterface;
import pl.gdziejaide.gjiandroid.actions.WhileLoopAction;

/**
 * Przetwórz element <while>
 * @author Przemek
 *
 */
public class WhileReader extends ObjectReaderBase {
	protected WhileLoopAction readWhile(Node whileNode) {
		WhileLoopAction whileAction = new WhileLoopAction();
		NodeList nodes = whileNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("conditions")) {
					ConditionsReader conditionsReader = new ConditionsReader();
					ConditionInterface condition = conditionsReader.readConditions(node);
					whileAction.setCondition(condition);
				} else if (node.getNodeName().equalsIgnoreCase("do")) {
					DoReader doReader = new DoReader();
					List<Action> actions = doReader.readDo(node);
					System.out.println(actions.size());
					whileAction.addActions(actions);
				} 
			}
		}
		return whileAction;
	}
}
