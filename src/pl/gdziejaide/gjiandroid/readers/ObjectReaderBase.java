package pl.gdziejaide.gjiandroid.readers;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Potrzebne metody do przetwarzania xml nodów
 *
 * @author Przemek
 *
 */
public abstract class ObjectReaderBase {
	protected Map<String, String> readAttrs(Node node, Map<String, String> names) {
		NamedNodeMap attrs = node.getAttributes();
		for (int i = 0; i < attrs.getLength(); i++) {
			Node attr = attrs.item(i);
			if (names.containsKey(attr.getNodeName())) {
				names.put(attr.getNodeName(), attr.getNodeValue());
			}
		}
		if (names.containsValue(null)) {
			System.err.println(names);
			throw new InvalidParameterException("W xml-u nie znaleziono wymaganego atrybutu w kluczu \"" + node.getNodeName() + "\"");
		}
		return names;
	}
	
	protected Map<String, String> createMap(String[] names) {
		return createMap(names, new String[names.length]);
	}
	
	protected Map<String, String> createMap(String[] names, String[] values) {
		Map<String, String> map = new TreeMap<String, String>();
		for (int i=0; i< names.length; i++) {
			map.put(names[i], values[i]);
		}
		return map;
	}
}
