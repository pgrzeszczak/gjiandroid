package pl.gdziejaide.gjiandroid.readers;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.utils.Point;

/**
 * Przetworzenie elementu <points>
 *
 * @author Przemek
 *
 */
public class PointsReader extends ObjectReaderBase {
	protected List<Point> readPoints(Node pointsNode) {
		List<Point> list =  new ArrayList<Point>();
		NodeList nodes = pointsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("point")) {
					PointReader pointReader = new PointReader();
					list.add(pointReader.readPoint(node));
				}
			}
		}
		return list;
	}
}
