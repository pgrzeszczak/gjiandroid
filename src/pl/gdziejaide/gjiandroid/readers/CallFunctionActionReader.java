package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.CallFunctionAction;
import pl.gdziejaide.gjiandroid.actions.Argument;
import pl.gdziejaide.gjiandroid.enums.PackageType;

/**
 * Przetwórz element <callFunction>
 * @author Przemek
 *
 */
public class CallFunctionActionReader extends ObjectReaderBase {
	protected CallFunctionAction readCallFunction(Node callFunctionActionNode) {
		Map<String, String> params = readAttrs(callFunctionActionNode, createMap(new String[] {
			"name", "target", "type"
		}, new String[] {
			null, "", null
		}));
		PackageType type = PackageType.getFromString(params.get("type"));
		CallFunctionAction callFunctionAction = CallFunctionAction.createForPackageType(
				type,
				params.get("name"),
				params.get("target"));
		NodeList nodes = callFunctionActionNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("arguments")) {
					ArgumentsReader argumentsReader = new ArgumentsReader();
					List<Argument> arguments = argumentsReader.readArguments(node);
					callFunctionAction.addArguments(arguments);
				}
			}
		}
		return callFunctionAction;
	}
}
