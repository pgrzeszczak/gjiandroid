package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.CallFunctionAction;
import pl.gdziejaide.gjiandroid.actions.ForLoopAction;
import pl.gdziejaide.gjiandroid.actions.IfStatementAction;
import pl.gdziejaide.gjiandroid.actions.WhileLoopAction;

/**
 * Przetwórz element <for>
 * @author Przemek
 *
 */
public class ForReader extends ObjectReaderBase {
	protected ForLoopAction readFor(Node forNode) {
		Map<String, String> params = readAttrs(forNode, createMap(new String[] {
			"endValue", "startValue", "step"
		}));
		ForLoopAction forAction = new ForLoopAction(
				Integer.parseInt(params.get("startValue")),
				Integer.parseInt(params.get("endValue")),
				Integer.parseInt(params.get("step")));
		NodeList nodes = forNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("callfunction")) {
					CallFunctionActionReader callFunctionReader = new CallFunctionActionReader();
					CallFunctionAction action = callFunctionReader.readCallFunction(node);
					forAction.addAction(action);
				} else if (node.getNodeName().equalsIgnoreCase("ifstatement")) {
					IfStatementReader ifStatementReader = new IfStatementReader();
					IfStatementAction action = ifStatementReader.readIfStatement(node);
					forAction.addAction(action);
				} else if (node.getNodeName().equalsIgnoreCase("for")) {
					ForReader forReader = new ForReader();
					ForLoopAction action = forReader.readFor(node);
					forAction.addAction(action);
				} else if (node.getNodeName().equalsIgnoreCase("while")) {
					WhileReader whileReader = new WhileReader();
					WhileLoopAction action = whileReader.readWhile(node);
					forAction.addAction(action);
				}
			}
		}
		return forAction;
	}
}
