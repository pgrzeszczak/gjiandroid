package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.enums.QuestState;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.Quest;

/**
 * Przetwórz element <character>
 * @author Przemek
 *
 */
public class QuestReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readQuest(Node questNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(questNode, createMap(new String[] {
			"id", "idname", "owned", "name", "description", "state", "icon", "visible"
		}, new String[] {
			null, null, "false", null, null, null, "-1", null
		}));
		Quest quest = new Quest(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("name"),
				params.get("description"),
				QuestState.getFromString(params.get("state")),
				Integer.parseInt(params.get("icon")),
				Boolean.valueOf(params.get("visible")));
		NodeList nodes = questNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						quest.registerEvent(event);
					}
				}
			}
		}
		list.put(quest.getId(), quest);
		return list;
	}
}
