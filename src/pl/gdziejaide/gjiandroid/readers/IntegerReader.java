package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.IntegerPrimitive;

/**
 * Przetwórz element <integer>
 * @author Przemek
 *
 */
public class IntegerReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readInteger(Node integerNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(integerNode, createMap(new String[] {
			"id", "idname", "owned", "value"
		}, new String[] {
			null, null, "false", null
		}));
		IntegerPrimitive integer = new IntegerPrimitive(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				Integer.parseInt(params.get("value")));
		NodeList nodes = integerNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						integer.registerEvent(event);
					}
				}
			}
		}
		list.put(integer.getId(), integer);
		return list;
	}
}
