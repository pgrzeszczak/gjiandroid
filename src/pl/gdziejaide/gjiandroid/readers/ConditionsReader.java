package pl.gdziejaide.gjiandroid.readers;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.ConditionInterface;

/**
 * Przetwórz element <conditions>
 * @author Przemek
 *
 */
public class ConditionsReader extends ObjectReaderBase {
	protected ConditionInterface readConditions(Node conditionsNode) {
		ConditionInterface condition = null;
		NodeList nodes = conditionsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("condition")) {
					ConditionReader conditionReader = new ConditionReader();
					condition = conditionReader.readCondition(node);
				} else if (node.getNodeName().equalsIgnoreCase("and")) {
					ConditionAndReader conditionandReader = new ConditionAndReader();
					condition = conditionandReader.readConditionAnd(node);
				} else if (node.getNodeName().equalsIgnoreCase("or")) {
					ConditionOrReader conditionOrReader = new ConditionOrReader();
					condition = conditionOrReader.readConditionOr(node);
				}
			}
		}
		return condition;
	}
}
