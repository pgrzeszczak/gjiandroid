package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <characters>
 * @author Przemek
 *
 */
public class CharactersReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readCharacters(Node charactersNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = charactersNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("character")) {
					CharacterReader characterReader = new CharacterReader();
					list.putAll(characterReader.readCharacter(node));
				}
			}
		}
		return list;
	}
}
