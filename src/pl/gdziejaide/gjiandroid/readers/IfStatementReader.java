package pl.gdziejaide.gjiandroid.readers;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.ConditionInterface;
import pl.gdziejaide.gjiandroid.actions.IfStatementAction;

/**
 * Przetwórz element <ifStatement>
 * @author Przemek
 *
 */
public class IfStatementReader extends ObjectReaderBase {
	protected IfStatementAction readIfStatement(Node ifStatementNode) {
		IfStatementAction action = new IfStatementAction();
		NodeList nodes = ifStatementNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("conditions")) {
					ConditionsReader conditionsReader = new ConditionsReader();
					ConditionInterface condition = conditionsReader.readConditions(node);
					action.setCondition(condition);
				} else if (node.getNodeName().equalsIgnoreCase("if")) {
					IfReader ifReader = new IfReader();
					action.addIfActions(ifReader.readIf(node));
				} else if (node.getNodeName().equalsIgnoreCase("else")) {
					ElseReader elseReader = new ElseReader();
					action.addElseActions(elseReader.readElse(node));
				}
			}
		}
		return action;
	}
}
