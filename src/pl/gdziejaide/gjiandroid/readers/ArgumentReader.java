package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;

import org.w3c.dom.Node;

import pl.gdziejaide.gjiandroid.actions.Argument;
import pl.gdziejaide.gjiandroid.enums.ArgumentType;

/**
 * Przetworzenie elementu <argument>
 *
 * @author Przemek
 *
 */
public class ArgumentReader extends ObjectReaderBase {
	protected Argument readArgument(Node argumentNode) {
		Map<String, String> params = readAttrs(argumentNode, createMap(new String[] {
			"type", "value",
		}));
		return new Argument(
				ArgumentType.getFromString(params.get("type")),
				params.get("value")
		);
	}
}
