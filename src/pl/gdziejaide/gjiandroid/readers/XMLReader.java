package pl.gdziejaide.gjiandroid.readers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwarza xml-a na obiekty
 *
 * @author Przemek
 *
 */
public class XMLReader {
	protected String xml;

	public XMLReader(String xml) {
		super();
		this.xml = xml;
	}

	public Map<Integer, GJIObject> createObjects() throws NumberFormatException, ParseException, SAXException, IOException, ParserConfigurationException {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(xml.getBytes()));
		NodeList nodes = doc.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node nod = nodes.item(i);
			if (nod.getNodeType() == Node.ELEMENT_NODE && nod.getNodeName().equalsIgnoreCase("scenario")) {
				ScenarioReader scenarioReader = new ScenarioReader();
				list.putAll(scenarioReader.readScenario(nod));
			}
		}
		return list;
	}
}
