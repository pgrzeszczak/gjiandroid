package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.enums.CharacterState;
import pl.gdziejaide.gjiandroid.enums.SexType;
import pl.gdziejaide.gjiandroid.objects.Character;
import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <character>
 * @author Przemek
 *
 */
public class CharacterReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readCharacter(Node characterNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(characterNode, createMap(new String[] {
			"id", "idname", "owned", "name", "sex", "description", "zone", "state", "visible", "icon"
		}, new String[] {
			null, null, "false", null, null, null, null, null, null, "-1"
		}));
		Character character = new Character(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("name"),
				SexType.getFromString(params.get("sex")),
				params.get("description"),
				Integer.parseInt(params.get("zone")),
				CharacterState.getFromString(params.get("state")),
				Boolean.valueOf(params.get("visible")),
				Integer.parseInt(params.get("icon")));
		NodeList nodes = characterNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						character.registerEvent(event);
					}
				}
			}
		}
		list.put(character.getId(), character);
		return list;
	}
}
