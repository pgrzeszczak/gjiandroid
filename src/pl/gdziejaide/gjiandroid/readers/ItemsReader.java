package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <items>
 * @author Przemek
 *
 */
public class ItemsReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readItems(Node itemsNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = itemsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("item")) {
					ItemReader itemReader = new ItemReader();
					list.putAll(itemReader.readItem(node));
				}
			}
		}
		return list;
	}
}
