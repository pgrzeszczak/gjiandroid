package pl.gdziejaide.gjiandroid.readers;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.Action;
import pl.gdziejaide.gjiandroid.actions.CallFunctionAction;
import pl.gdziejaide.gjiandroid.actions.ForLoopAction;
import pl.gdziejaide.gjiandroid.actions.IfStatementAction;
import pl.gdziejaide.gjiandroid.actions.WhileLoopAction;

/**
 * Przetwórz element <if>
 * @author Przemek
 *
 */
public class IfReader extends ObjectReaderBase {
	protected List<Action> readIf(Node ifNode) {
		List<Action> list = new ArrayList<Action>();
		NodeList nodes = ifNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("callfunction")) {
					CallFunctionActionReader callFunctionReader = new CallFunctionActionReader();
					CallFunctionAction action = callFunctionReader.readCallFunction(node);
					list.add(action);
				} else if (node.getNodeName().equalsIgnoreCase("ifstatement")) {
					IfStatementReader ifStatementReader = new IfStatementReader();
					IfStatementAction action = ifStatementReader.readIfStatement(node);
					list.add(action);
				} else if (node.getNodeName().equalsIgnoreCase("for")) {
					ForReader forReader = new ForReader();
					ForLoopAction action = forReader.readFor(node);
					list.add(action);
				} else if (node.getNodeName().equalsIgnoreCase("while")) {
					WhileReader whileReader = new WhileReader();
					WhileLoopAction action = whileReader.readWhile(node);
					list.add(action);
				}
			}
		}
		return list;
	}
}
