package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;

import org.w3c.dom.Node;

import pl.gdziejaide.gjiandroid.actions.Argument;
import pl.gdziejaide.gjiandroid.actions.Condition;
import pl.gdziejaide.gjiandroid.actions.ConditionInterface;
import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.ConditionOperatorType;

/**
 * Przetworzenie elementu <condition>
 *
 * @author Przemek
 *
 */
public class ConditionReader extends ObjectReaderBase {
	protected ConditionInterface readCondition(Node conditionNode) {
		Map<String, String> params = readAttrs(conditionNode, createMap(new String[] {
			"eastSide", "eastSideType", "operator", "westSide", "westSideType"
		}));
		Condition condition = new Condition();
		Argument east = new Argument(
				ArgumentType.getFromString(params.get("eastSideType")),
				params.get("eastSide")
		);
		Argument west = new Argument(
				ArgumentType.getFromString(params.get("westSideType")),
				params.get("westSide")
		);
		ConditionOperatorType operator = ConditionOperatorType.getFromString(params.get("operator"));
		condition.setLeftArgument(east);
		condition.setRightArgument(west);
		condition.setOperatorType(operator);
		return condition;
	}
}
