package pl.gdziejaide.gjiandroid.readers;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;

/**
 * Przetwórz element <events>
 * @author Przemek
 *
 */
public class EventsReader extends ObjectReaderBase {
	protected List<EventAction> readEvents(Node eventsNode) {
		List<EventAction> list = new ArrayList<EventAction>();
		NodeList nodes = eventsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("event")) {
					EventReader eventReader = new EventReader();
					EventAction event = eventReader.readEvent(node);
					list.add(event);
				}
			}
		}
		return list;
	}
}
