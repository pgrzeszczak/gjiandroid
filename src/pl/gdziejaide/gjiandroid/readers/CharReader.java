package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.CharPrimitive;
import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <char>
 * @author Przemek
 *
 */
public class CharReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readChar(Node charNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(charNode, createMap(new String[] {
			"id", "idname", "owned", "value"
		}, new String[] {
			null, null, "false", null
		}));
		CharPrimitive charP = new CharPrimitive(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("value").charAt(0));
		NodeList nodes = charNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						charP.registerEvent(event);
					}
				}
			}
		}
		list.put(charP.getId(), charP);
		return list;
	}
}
