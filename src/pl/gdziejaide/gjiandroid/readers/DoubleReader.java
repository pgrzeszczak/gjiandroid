package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.DoublePrimitive;
import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <double>
 * @author Przemek
 *
 */
public class DoubleReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readDouble(Node doubleNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(doubleNode, createMap(new String[] {
			"id", "idname", "owned", "value"
		}, new String[] {
			null, null, "false", null
		}));
		DoublePrimitive doubleP = new DoublePrimitive(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				Double.parseDouble(params.get("value")));
		NodeList nodes = doubleNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						doubleP.registerEvent(event);
					}
				}
			}
		}
		list.put(doubleP.getId(), doubleP);
		return list;
	}
}
