package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.Timer;

/**
 * Przetwórz element <timer>
 * @author Przemek
 *
 */
public class TimerReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readTimer(Node timerNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(timerNode, createMap(new String[] {
			"id", "idname", "owned", "interval"
		}, new String[] {
			null, null, "false", null
		}));
		Timer timer = new Timer(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				Integer.parseInt(params.get("interval")));
		NodeList nodes = timerNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						timer.registerEvent(event);
					}
				}
			}
		}
		list.put(timer.getId(), timer);
		return list;
	}
}
