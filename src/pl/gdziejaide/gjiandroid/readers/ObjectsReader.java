package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <objects>
 *
 * @author Przemek
 *
 */
public class ObjectsReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readObjects(Node objectsNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = objectsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("zones")) {
					ZonesReader zonesReader = new ZonesReader();
					list.putAll(zonesReader.readZones(node));
				} else if (node.getNodeName().equalsIgnoreCase("characters")) {
					CharactersReader charactersReader = new CharactersReader();
					list.putAll(charactersReader.readCharacters(node));
				} else if (node.getNodeName().equalsIgnoreCase("items")) {
					ItemsReader itemsReader = new ItemsReader();
					list.putAll(itemsReader.readItems(node));
				} else if (node.getNodeName().equalsIgnoreCase("quests")) {
					QuestsReader questsReader = new QuestsReader();
					list.putAll(questsReader.readQuests(node));
				} else if (node.getNodeName().equalsIgnoreCase("medias")) {
					MediasReader mediasReader = new MediasReader();
					list.putAll(mediasReader.readMedias(node));
				} else if (node.getNodeName().equalsIgnoreCase("timers")) {
					TimersReader timersReader = new TimersReader();
					list.putAll(timersReader.readTimers(node));
				} else if (node.getNodeName().equalsIgnoreCase("primitives")) {
					PrimitivesReader primitivesReader = new PrimitivesReader();
					list.putAll(primitivesReader.readPrimitives(node));
				}
			}
		}
		return list;
	}
}
