package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.StringPrimitive;

/**
 * Przetwórz element <string>
 * @author Przemek
 *
 */
public class StringReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readString(Node stringNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(stringNode, createMap(new String[] {
			"id", "idname", "owned", "value"
		}, new String[] {
			null, null, "false", null
		}));
		StringPrimitive string = new StringPrimitive(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("value"));
		NodeList nodes = stringNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						string.registerEvent(event);
					}
				}
			}
		}
		list.put(string.getId(), string);
		return list;
	}
}
