package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <medias>
 * @author Przemek
 *
 */
public class MediasReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readMedias(Node mediasNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = mediasNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("media")) {
					MediaReader mediaReader = new MediaReader();
					list.putAll(mediaReader.readMedia(node));
				}
			}
		}
		return list;
	}
}
