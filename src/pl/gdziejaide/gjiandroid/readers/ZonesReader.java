package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <zones>
 * @author Przemek
 *
 */
public class ZonesReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readZones(Node zonesNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = zonesNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("zone")) {
					ZoneReader zoneReader = new ZoneReader();
					list.putAll(zoneReader.readZone(node));
				}
			}
		}
		return list;
	}
}
