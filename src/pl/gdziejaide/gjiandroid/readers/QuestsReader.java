package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <quests>
 * @author Przemek
 *
 */
public class QuestsReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readQuests(Node questsNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = questsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("quest")) {
					QuestReader questReader = new QuestReader();
					list.putAll(questReader.readQuest(node));
				}
			}
		}
		return list;
	}
}
