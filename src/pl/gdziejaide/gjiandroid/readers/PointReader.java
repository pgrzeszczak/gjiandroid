package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;

import org.w3c.dom.Node;

import pl.gdziejaide.gjiandroid.utils.Point;

/**
 * Przetworzenie elementu <point>
 *
 * @author Przemek
 *
 */
public class PointReader extends ObjectReaderBase {
	protected Point readPoint(Node pointNode) {
		Map<String, String> params = readAttrs(pointNode, createMap(new String[] {
			"latitude", "longitude",
		}));
//		System.out.println(params.get("latitude") + " " + Double.parseDouble(params.get("latitude"))*1000000 + " " + Math.round(Double.parseDouble(params.get("latitude"))*1000000) + " " + (int) Math.round(Double.parseDouble(params.get("latitude"))*1000000));
//		System.out.println(params.get("longitude") + " " + Double.parseDouble(params.get("longitude"))*1000000 + " " + Math.round(Double.parseDouble(params.get("longitude"))*1000000) + " " + (int) Math.round(Double.parseDouble(params.get("longitude"))*1000000));
		return new Point(
				(int) Math.round(Double.parseDouble(params.get("latitude"))*1000000),
				(int) Math.round(Double.parseDouble(params.get("longitude"))*1000000)
		);
	}
}
