package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.enums.MediaType;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.ImageMedia;
import pl.gdziejaide.gjiandroid.objects.Media;
import pl.gdziejaide.gjiandroid.objects.MusicMedia;
import pl.gdziejaide.gjiandroid.objects.VideoMedia;

/**
 * Przetwórz element <media>
 * @author Przemek
 *
 */
public class MediaReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readMedia(Node mediaNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(mediaNode, createMap(new String[] {
			"id", "idname", "owned", "path", "zone", "type"
		}, new String[] {
				null, null, "false", null, "", null
		}));
		MediaType type = MediaType.getFromString(params.get("type"));
		Media media = null;
		switch (type) {
			case IMAGE: media = new ImageMedia(
					Integer.parseInt(params.get("id")),
					params.get("idname"),
					Boolean.valueOf(params.get("owned")),
					params.get("path"),
					params.get("zone").equals("") ? -1 : Integer.parseInt(params.get("zone")));
					break;
			case MUSIC: media = new MusicMedia(
					Integer.parseInt(params.get("id")),
					params.get("idname"),
					Boolean.valueOf(params.get("owned")),
					params.get("path"),
					params.get("zone").equals("") ? -1 : Integer.parseInt(params.get("zone")));
					break;
			case VIDEO: media = new VideoMedia(
					Integer.parseInt(params.get("id")),
					params.get("idname"),
					Boolean.valueOf(params.get("owned")),
					params.get("path"),
					params.get("zone").equals("") ? -1 : Integer.parseInt(params.get("zone")));
					break;
			default: return list;
		}
		NodeList nodes = mediaNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						media.registerEvent(event);
					}
				}
			}
		}
		list.put(media.getId(), media);
		return list;
	}
}
