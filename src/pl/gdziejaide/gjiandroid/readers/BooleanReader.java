package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.BooleanPrimitive;
import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <boolean>
 * @author Przemek
 *
 */
public class BooleanReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readBoolean(Node booleanNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(booleanNode, createMap(new String[] {
			"id", "idname", "owned", "value"
		}, new String[] {
			null, null, "false", null
		}));
		BooleanPrimitive booleanP = new BooleanPrimitive(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				Boolean.valueOf(params.get("value")));
		NodeList nodes = booleanNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						booleanP.registerEvent(event);
					}
				}
			}
		}
		list.put(booleanP.getId(), booleanP);
		return list;
	}
}
