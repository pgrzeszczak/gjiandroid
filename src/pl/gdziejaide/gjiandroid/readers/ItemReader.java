package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.enums.ItemState;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.Item;

/**
 * Przetwórz element <item>
 * @author Przemek
 *
 */
public class ItemReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readItem(Node itemNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(itemNode, createMap(new String[] {
			"id", "idname", "owned", "name", "description", "zone", "visible", "state", "icon"
		}, new String[] {
			null, null, "false", null, null, null, null, null, "-1"
		}));
		Item item = new Item(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("name"),
				params.get("description"),
				Integer.parseInt(params.get("zone")),
				Boolean.valueOf(params.get("visible")),
				ItemState.getFromString(params.get("state")),
				Integer.parseInt(params.get("icon")));
		NodeList nodes = itemNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						item.registerEvent(event);
					}
				}
			}
		}
		list.put(item.getId(), item);
		return list;
	}
}
