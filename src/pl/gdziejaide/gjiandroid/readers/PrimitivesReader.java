package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <primitives>
 * @author Przemek
 *
 */
public class PrimitivesReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readPrimitives(Node primitivesNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = primitivesNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("integer")) {
					IntegerReader integerReader = new IntegerReader();
					list.putAll(integerReader.readInteger(node));
				} else if (node.getNodeName().equalsIgnoreCase("string")) {
					StringReader stringReader = new StringReader();
					list.putAll(stringReader.readString(node));
				} else if (node.getNodeName().equalsIgnoreCase("boolean")) {
					BooleanReader booleanReader = new BooleanReader();
					list.putAll(booleanReader.readBoolean(node));
				} else if (node.getNodeName().equalsIgnoreCase("double")) {
					DoubleReader doubleReader = new DoubleReader();
					list.putAll(doubleReader.readDouble(node));
				} else if (node.getNodeName().equalsIgnoreCase("char")) {
					CharReader charReader = new CharReader();
					list.putAll(charReader.readChar(node));
				}
			}
		}
		return list;
	}
}
