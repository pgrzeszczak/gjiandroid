package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.objects.GJIObject;

/**
 * Przetwórz element <timers>
 * @author Przemek
 *
 */
public class TimersReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readTimers(Node timersNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		NodeList nodes = timersNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("timer")) {
					TimerReader timerReader = new TimerReader();
					list.putAll(timerReader.readTimer(node));
				}
			}
		}
		return list;
	}
}
