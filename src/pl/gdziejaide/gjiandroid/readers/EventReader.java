package pl.gdziejaide.gjiandroid.readers;

import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.CallFunctionAction;
import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.actions.ForLoopAction;
import pl.gdziejaide.gjiandroid.actions.IfStatementAction;
import pl.gdziejaide.gjiandroid.actions.WhileLoopAction;
import pl.gdziejaide.gjiandroid.enums.EventType;

/**
 * Przetwórz element <event>
 * @author Przemek
 *
 */
public class EventReader extends ObjectReaderBase {
	protected EventAction readEvent(Node eventNode) {
		Map<String, String> params = readAttrs(eventNode, createMap(new String[] {
			"type"
		}));
		EventAction event = new EventAction(
				EventType.getFromString(params.get("type")));
		NodeList nodes = eventNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("callfunction")) {
					CallFunctionActionReader callFunctionReader = new CallFunctionActionReader();
					CallFunctionAction action = callFunctionReader.readCallFunction(node);
					event.addSubAction(action);
				} else if (node.getNodeName().equalsIgnoreCase("ifstatement")) {
					IfStatementReader ifStatementReader = new IfStatementReader();
					IfStatementAction action = ifStatementReader.readIfStatement(node);
					event.addSubAction(action);
				} else if (node.getNodeName().equalsIgnoreCase("for")) {
					ForReader forReader = new ForReader();
					ForLoopAction action = forReader.readFor(node);
					event.addSubAction(action);
				} else if (node.getNodeName().equalsIgnoreCase("while")) {
					WhileReader whileReader = new WhileReader();
					WhileLoopAction action = whileReader.readWhile(node);
					event.addSubAction(action);
				}
			}
		}
		return event;
	}
}
