package pl.gdziejaide.gjiandroid.readers;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.Argument;

/**
 * Przetwórz element <arguments>
 * @author Przemek
 *
 */
public class ArgumentsReader extends ObjectReaderBase {
	protected List<Argument> readArguments(Node argumentsNode) {
		List<Argument> list =  new ArrayList<Argument>();
		NodeList nodes = argumentsNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("argument")) {
					ArgumentReader argumentReader = new ArgumentReader();
					Argument argument = argumentReader.readArgument(node);
					list.add(argument);
				}
			}
		}
		return list;
	}
}
