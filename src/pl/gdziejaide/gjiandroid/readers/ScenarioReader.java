package pl.gdziejaide.gjiandroid.readers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.Scenario;

/**
 * Przetwórz node <scenario> na obiekt Scenario
 *
 * @author Przemek
 *
 */
public class ScenarioReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readScenario(Node scenarioNode) throws NumberFormatException, ParseException {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(scenarioNode, createMap(new String[] {
			"id", "idname", "owned", "name", "author", "created", "creatorVersion", "description", "version"	
		}, new String[] {
			null, null, "false", null, null, null, null, null, null
		}));
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Scenario scenario = new Scenario(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("name"),
				params.get("description"),
				params.get("author"),
				params.get("creatorVersion"),
				params.get("version"),
				formatter.parse(params.get("created")));
		list.put(scenario.getId(), scenario);
		NodeList nodes = scenarioNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("objects")) {
					ObjectsReader objectsReader = new ObjectsReader();
					list.putAll(objectsReader.readObjects(node));
				} else if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						scenario.registerEvent(event);
					}
				}
			}
		}
		return list;
	}
}
