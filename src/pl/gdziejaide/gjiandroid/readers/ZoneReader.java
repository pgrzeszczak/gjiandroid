package pl.gdziejaide.gjiandroid.readers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.EventAction;
import pl.gdziejaide.gjiandroid.enums.ZoneState;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.Zone;

/**
 * Przetwórz element <zone>
 * @author Przemek
 *
 */
public class ZoneReader extends ObjectReaderBase {
	protected Map<Integer, GJIObject> readZone(Node zoneNode) {
		Map<Integer, GJIObject> list =  new TreeMap<Integer, GJIObject>();
		Map<String, String> params = readAttrs(zoneNode, createMap(new String[] {
			"id", "idname", "owned", "name", "description", "radius", "state", "visible", "icon"
		}, new String[] {
			null, null, "false", null, null, "-1", null, null, "-1"
		}));
		Zone zone = new Zone(
				Integer.parseInt(params.get("id")),
				params.get("idname"),
				Boolean.valueOf(params.get("owned")),
				params.get("name"),
				params.get("description"),
				params.get("radius").equals("") ? -1 : Integer.parseInt(params.get("radius")),
				ZoneState.getFromString(params.get("state")),
				Boolean.valueOf(params.get("visible")),
				Integer.parseInt(params.get("icon")));
		NodeList nodes = zoneNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("points")) {
					PointsReader pointsReader = new PointsReader();
					zone.setPoints(pointsReader.readPoints(node));
				} else if (node.getNodeName().equalsIgnoreCase("events")) {
					EventsReader eventsReader = new EventsReader();
					List<EventAction> events = eventsReader.readEvents(node);
					for (EventAction event: events) {
						zone.registerEvent(event);
					}
				}
			}
		}
		list.put(zone.getId(), zone);
		return list;
	}
}
