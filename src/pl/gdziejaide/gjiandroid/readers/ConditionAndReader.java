package pl.gdziejaide.gjiandroid.readers;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.actions.ConditionAnd;
import pl.gdziejaide.gjiandroid.actions.ConditionInterface;

/**
 * Przetwórz element <and>
 * @author Przemek
 *
 */
public class ConditionAndReader extends ObjectReaderBase {
	protected ConditionInterface readConditionAnd(Node conditionAndNode) {
		ConditionAnd condition = new ConditionAnd();
		NodeList nodes = conditionAndNode.getChildNodes();
		for (int i=0; i< nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("condition")) {
					ConditionReader conditionReader = new ConditionReader();
					condition.addCondition(conditionReader.readCondition(node));
				} else if (node.getNodeName().equalsIgnoreCase("and")) {
					ConditionAndReader conditionAndReader = new ConditionAndReader();
					condition.addCondition(conditionAndReader.readConditionAnd(node));
				} else if (node.getNodeName().equalsIgnoreCase("or")) {
					ConditionOrReader conditionOrReader = new ConditionOrReader();
					condition.addCondition(conditionOrReader.readConditionOr(node));
				}
			}
		}
		return condition;
	}
}
