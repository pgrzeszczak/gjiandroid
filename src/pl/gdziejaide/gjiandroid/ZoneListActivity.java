package pl.gdziejaide.gjiandroid;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.map.MyMapView;
import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Zone;
import pl.gdziejaide.gjiandroid.utils.Point;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;

public class ZoneListActivity extends MyActivity {

	List<Zone> zones = new ArrayList<Zone>();

	private class ZoneListAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		public ZoneListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		@Override
		public int getCount() {
			return zones.size();
		}

		@Override
		public Object getItem(int position) {
			return zones.get(position);
		}

		@Override
		public long getItemId(int position) {
			return zones.get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;

			if (convertView == null)
				vi = inflater.inflate(R.layout.item_list_row, null);
			TextView tv1 = (TextView) vi.findViewById(R.id.textView1);

			Zone zone = (Zone) getItem(position);

			tv1.setText(zone.getName());

			return vi;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_zone_list);
		setDimensions();

		for (Zone zone : ObjectsContainer.getZones()) {
			if (zone.visible == true) {
				zones.add(zone);
			}
		}
		ZoneListAdapter adapter = new ZoneListAdapter(this);
		ListView lv = (ListView) findViewById(R.id.listView1);
		lv.setAdapter(adapter);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
				showZoneInfo(zones.get(arg2).getId());
			}
		});
	}

	public void animateToZone(Zone zone) {
		int lt = 0, ln = 0;
		for (Point p : zone.getPoints()) {
			lt += p.getLatitude();
			ln += p.getLongitude();
		}
		lt /= zone.getPoints().size();
		ln /= zone.getPoints().size();
		GeoPoint p = new GeoPoint(lt, ln);
		MyMapView.mapa.animateToPoint(p);
	}

	public void finishHim(View view) {
		finish();
	}
	
	public void showZoneInfo(int zoneId) {
		Intent intent = new Intent(this, ZoneInfoActivity.class);
		intent.putExtra("ZONE_ID", zoneId);
		startActivity(intent);
		finish();
	}

}
