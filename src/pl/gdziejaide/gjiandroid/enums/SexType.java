package pl.gdziejaide.gjiandroid.enums;

public enum SexType {
	MALE, FEMALE, OTHER;
	
	public static SexType getFromString(String type) {
		if (type.equalsIgnoreCase("male")) {
			return MALE;
		} else if (type.equalsIgnoreCase("female")) {
			return FEMALE;
		}
		return OTHER;
	}
}
