package pl.gdziejaide.gjiandroid.enums;

public enum ObjectType {
	SCENARIO, BOOLEAN, CHARACTER, CHAR, DOUBLE,
	IMAGE, INTEGER, ITEM, MUSIC, QUEST, STRING,
	TIMER, VIDEO, ZONE;
}
