package pl.gdziejaide.gjiandroid.enums;

public enum MediaType {
	UNKNOWN, IMAGE, MUSIC, VIDEO;
	
	public static MediaType getFromString(String type) {
		if (type.equalsIgnoreCase("image")) {
			return IMAGE;
		} else if (type.equalsIgnoreCase("music")) {
			return MUSIC;
		} else if (type.equalsIgnoreCase("video")) {
			return VIDEO;
		}
		return UNKNOWN;
	}
}
