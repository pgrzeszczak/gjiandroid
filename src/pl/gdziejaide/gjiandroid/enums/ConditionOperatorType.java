package pl.gdziejaide.gjiandroid.enums;

public enum ConditionOperatorType {
	UNKNOWN, EQUAL, LESS, GREATER, NOT_EQUAL, GREATER_EQUAL, LESS_EQUAL;
	
	public static ConditionOperatorType getFromString(String type) {
		if (type.equalsIgnoreCase("equal") || type.equalsIgnoreCase("==")) {
			return EQUAL;
		} else if (type.equalsIgnoreCase("less") || type.equalsIgnoreCase("<")) {
			return LESS;
		} else if (type.equalsIgnoreCase("greater") || type.equalsIgnoreCase(">")) {
			return GREATER;
		} else if (type.equalsIgnoreCase("notequal") || type.equalsIgnoreCase("!=")) {
			return NOT_EQUAL;
		} else if (type.equalsIgnoreCase("greaterequal") || type.equalsIgnoreCase(">=")) {
			return GREATER_EQUAL;
		} else if (type.equalsIgnoreCase("lessequal") || type.equalsIgnoreCase("<=")) {
			return LESS_EQUAL;
		}
		return UNKNOWN;
	}
}
