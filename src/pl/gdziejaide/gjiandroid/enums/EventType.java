package pl.gdziejaide.gjiandroid.enums;

public enum EventType {
	UNKNOWN, BEGIN, END, RESTORE, SAVE,
	ENTER, LEAVE, GET_CLOSER, VISIBLE_CHANGE, STATE_CHANGE,
	GOOD_MORNING, GOOD_BYE, ZONE_CHANGE, DIALOG,
	FIND, DROP, PICK,
	COMPLETE, INTERACTION,
	PLAY, SHOW,
	START, STOP, TIMER,
	VALUE_CHANGE;
	
	public static EventType getFromString(String type)
	{
		if (type.equalsIgnoreCase("onbegin")) {
			return BEGIN;
		} else if (type.equalsIgnoreCase("onend")) {
			return END;
		} else if (type.equalsIgnoreCase("onrestore")) {
			return RESTORE;
		} else if (type.equalsIgnoreCase("onsave")) {
			return SAVE;
		} else if (type.equalsIgnoreCase("onenter")) {
			return ENTER;
		} else if (type.equalsIgnoreCase("onleave")) {
			return LEAVE;
		} else if (type.equalsIgnoreCase("ongetcloser")) {
			return GET_CLOSER;
		} else if (type.equalsIgnoreCase("onvisiblechange")) {
			return VISIBLE_CHANGE;
		} else if (type.equalsIgnoreCase("onstatechange")) {
			return STATE_CHANGE;
		} else if (type.equalsIgnoreCase("ongoodmorning")) {
			return GOOD_MORNING;
		} else if (type.equalsIgnoreCase("ongoodbye")) {
			return GOOD_BYE;
		} else if (type.equalsIgnoreCase("onzonechange")) {
			return ZONE_CHANGE;
		} else if (type.equalsIgnoreCase("ondialog")) {
			return DIALOG;
		} else if (type.equalsIgnoreCase("onfind")) {
			return FIND;
		} else if (type.equalsIgnoreCase("ondrop")) {
			return DROP;
		} else if (type.equalsIgnoreCase("onpick")) {
			return PICK;
		} else if (type.equalsIgnoreCase("onplay")) {
			return PLAY;
		} else if (type.equalsIgnoreCase("onshow")) {
			return SHOW;
		} else if (type.equalsIgnoreCase("oncomplete")) {
			return COMPLETE;
		} else if (type.equalsIgnoreCase("oninteraction")) {
			return INTERACTION;
		} else if (type.equalsIgnoreCase("onstart")) {
			return START;
		} else if (type.equalsIgnoreCase("onstop")) {
			return STOP;
		} else if (type.equalsIgnoreCase("ontimer")) {
			return TIMER;
		} else if (type.equalsIgnoreCase("onvaluechange")) {
			return VALUE_CHANGE;
		} else {
			return UNKNOWN;
		}
	}
}
