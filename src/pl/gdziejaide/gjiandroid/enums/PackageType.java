package pl.gdziejaide.gjiandroid.enums;

public enum PackageType {
	UNKNOWN, GUI, MATH, STRINGS, UTILS;
	
	public static PackageType getFromString(String type) {
		if (type.equalsIgnoreCase("gui")) {
			return GUI;
		} else if (type.equalsIgnoreCase("math")) {
			return MATH;
		} else if (type.equalsIgnoreCase("strings")) {
			return STRINGS;
		} else if (type.equalsIgnoreCase("utils")) {
			return UTILS;
		}
		return UNKNOWN;
	}
}
