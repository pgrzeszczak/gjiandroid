package pl.gdziejaide.gjiandroid.enums;

public enum ArgumentType {
	UNKNOWN, VALUE, INTEGER, STRING, BOOLEAN, DOUBLE, CHAR;
	
	public static ArgumentType getFromString(String type) {
		if (type.equalsIgnoreCase("value")) {
			return VALUE;
		} else if (type.equalsIgnoreCase("integer")) {
			return INTEGER;
		} else if (type.equalsIgnoreCase("string")) {
			return STRING;
		} else if (type.equalsIgnoreCase("boolean")) {
			return BOOLEAN;
		} else if (type.equalsIgnoreCase("double")) {
			return DOUBLE;
		} else if (type.equalsIgnoreCase("char")) {
			return CHAR;
		}
		return UNKNOWN;
	}
}
