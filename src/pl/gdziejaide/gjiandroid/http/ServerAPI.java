package pl.gdziejaide.gjiandroid.http;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.gdziejaide.gjiandroid.ScenarioHeader;
import android.os.AsyncTask;
import android.os.Environment;

public class ServerAPI {
	public static String username = null;
	public static String password = null;
	protected static Map<String, String> serverLinks = new HashMap<String, String>();

	protected ServerAPI() {
	}

	public static void addLinks(Map<String, String> serverLinks) {
		ServerAPI.serverLinks.putAll(serverLinks);
	}

	public static void clearCredentials() {
		username = null;
		password = null;
	}

	public static void setCredentials(String username, String password) {
		ServerAPI.username = username;
		ServerAPI.password = password;
	}

	protected static void getResponse(String urlString,
			final StringResponseCallback callback) {
		new AsyncTask<String, Void, String>() {
			protected String doInBackground(String... urls) {
				try {
					URL url = new URL(urls[0]);
					String encoding = "";
					if (password != null && username != null) {
						encoding = MyBase64.encode((username + ":" + password)
								.getBytes());
					}

					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					connection.setRequestMethod("GET");
					// connection.setDoOutput(true);
					// System.out.println(connection.getResponseCode());
					if (encoding != "") {
						connection.setRequestProperty("Authorization", "Basic "
								+ encoding);
					}
					InputStream content = (InputStream) connection
							.getInputStream();
					BufferedReader in = new BufferedReader(
							new InputStreamReader(content));
					String line;
					StringBuffer buf = new StringBuffer();
					while ((line = in.readLine()) != null) {
						buf.append(line);
					}
					return buf.toString();
				} catch (IOException e) {
					e.printStackTrace();
					System.err.println("Błąd http");
					return "ERROR";
				}
			}

			protected void onPostExecute(String response) {
				callback.stringCallback(response);
			}
		}.execute(urlString);
	}

	protected static void getFileResponse(String urlString,
			final String savePath, final FileResponseCallback callback) {
		new AsyncTask<String, Void, File>() {
			protected File doInBackground(String... urls) {
				try {
					URL url = new URL(urls[0]);
					String encoding = "";
					if (password != null && username != null) {
						encoding = MyBase64.encode((username + ":" + password)
								.getBytes());
					}

					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					connection.setRequestMethod("GET");
					// connection.setDoOutput(true);
					// System.out.println(connection.getResponseCode());
					if (encoding != "") {
						connection.setRequestProperty("Authorization", "Basic "
								+ encoding);
					}
					InputStream inputStream = connection.getInputStream();
					String fileName = "";
					String rawHeader = connection
							.getHeaderField("Content-Disposition");
					// raw = "attachment; filename="abc.jpg""
					if (rawHeader != null && rawHeader.indexOf("=") != -1) {
						fileName = rawHeader.substring(
								rawHeader.lastIndexOf('=') + 1).replace("\"",
								"");
					} else {
						throw new IOException("Invalid filename in header");
					}
					File file = new File(savePath + "/" + fileName);
					file.getParentFile().mkdirs();
					FileOutputStream fileOutput = new FileOutputStream(file);
					int totalSize = connection.getContentLength();
					int downloadedSize = 0;
					byte[] buffer = new byte[1024];
					int bufferLength = 0;
					while ((bufferLength = inputStream.read(buffer)) > 0) {
						fileOutput.write(buffer, 0, bufferLength);
						downloadedSize += bufferLength;
						callback.progressCallback(totalSize, downloadedSize);

					}
					fileOutput.close();
					return file;
				} catch (IOException e) {
					e.printStackTrace();
					System.err.println("Błąd http");
					return null;
				}
			}

			protected void onPostExecute(File response) {
				callback.fileCallback(response);
			}
		}.execute(urlString);
	}

	public static void getPublicList(StringResponseCallback callback,
			int start, int howmany) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_publiclist") + "/" + start + "/"
				+ howmany;
		getResponse(url, callback);
	}

	public static void getPublicList(StringResponseCallback callback) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_publiclist");
		getResponse(url, callback);
	}

	public static void getUserScenarios(StringResponseCallback callback,
			int start, int howmany) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_userscenarios") + "/" + start
				+ "/" + howmany;
		getResponse(url, callback);
	}

	public static void getUserScenarios(StringResponseCallback callback) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_userscenarios");
		getResponse(url, callback);
	}

	public static void getSplash(int scenarioId, FileResponseCallback callback) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_getsplash") + "/" + scenarioId;
		File SDCardRoot = Environment.getExternalStorageDirectory();
		getFileResponse(url, SDCardRoot.getAbsolutePath() + "/gji/splash",
				callback);
	}

	public static void getScenario(int scenarioId, FileResponseCallback callback) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_getscenario") + "/" + scenarioId;
		File SDCardRoot = Environment.getExternalStorageDirectory();
		String scenarioRoot = SDCardRoot.getAbsolutePath() + "/gji/scenario";
		getScenarioCommon(url, scenarioRoot, callback);
	}

	public static void getScenario(int scenarioId, String pass,
			FileResponseCallback callback) {
		String url = serverLinks.get("server_url")
				+ serverLinks.get("server_url_getscenario") + "/" + scenarioId
				+ "/" + pass;
		File SDCardRoot = Environment.getExternalStorageDirectory();
		String scenarioRoot = SDCardRoot.getAbsolutePath() + "/gji/scenario";
		getScenarioCommon(url, scenarioRoot, callback);
	}

	private static void getScenarioCommon(String url,
			final String scenarioRoot, final FileResponseCallback callback) {
		getFileResponse(url, scenarioRoot, new FileResponseCallback() {

			@Override
			public void progressCallback(int size, int downloaded) {
				callback.progressCallback(size, downloaded);
			}

			@Override
			public void fileCallback(File response) {
				if (response == null) {
					callback.fileCallback(null);
					return;
				}
				String folderPath = scenarioRoot
						+ "/"
						+ response.getName().substring(0,
								response.getName().lastIndexOf("."));
				System.out.println(folderPath);
				try {
					ZipFile zipFile = new ZipFile(response);

					Enumeration entries = zipFile.entries();

					while (entries.hasMoreElements()) {
						ZipEntry entry = (ZipEntry) entries.nextElement();

						if (entry.isDirectory()) {
							System.out.println("Extracting directory: "
									+ entry.getName());
							(new File(folderPath + "/" + entry.getName()))
									.mkdirs();
							continue;
						}

						System.out.println("Extracting file: "
								+ entry.getName());
						byte[] buffer = new byte[1024];
						InputStream in = zipFile.getInputStream(entry);
						(new File(folderPath + "/" + entry.getName()))
								.getParentFile().mkdirs();
						OutputStream out = new BufferedOutputStream(
								new FileOutputStream(folderPath + "/"
										+ entry.getName()));
						int len;
						while ((len = in.read(buffer)) >= 0)
							out.write(buffer, 0, len);
						in.close();
						out.close();
					}

					zipFile.close();
					response.delete();
					callback.fileCallback(new File(folderPath));
				} catch (IOException ioe) {
					response.delete();
					System.err.println("Blad rozpakowywania");
					ioe.printStackTrace();
					callback.fileCallback(null);
				}
			}
		});
	}

	public static ArrayList<ScenarioHeader> XmlListToScenarioHeaderList(
			String xml) {
		ArrayList<ScenarioHeader> list = new ArrayList<ScenarioHeader>();
		try {
			Document doc = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder()
					.parse(new ByteArrayInputStream(xml.getBytes()));
			NodeList rootNodes = doc.getChildNodes();
			for (int i = 0; i < rootNodes.getLength(); i++) {
				Node rootNode = rootNodes.item(i);
				NodeList nodes = rootNode.getChildNodes();
				for (int j = 0; j < nodes.getLength(); j++) {
					Node node = nodes.item(j);
					if (node.getNodeType() != Node.TEXT_NODE) {
						NamedNodeMap attrs = node.getAttributes();
						ScenarioHeader header = new ScenarioHeader(
								Integer.parseInt(attrs.getNamedItem("id")
										.getNodeValue()), attrs.getNamedItem(
										"name").getNodeValue(), attrs
										.getNamedItem("description")
										.getNodeValue(), attrs.getNamedItem(
										"author").getNodeValue(), attrs
										.getNamedItem("created").getNodeValue());
						list.add(header);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
