package pl.gdziejaide.gjiandroid.http;

import java.io.File;

public interface FileResponseCallback {
	public void fileCallback(File response);
	public void progressCallback(int size, int downloaded);
}
