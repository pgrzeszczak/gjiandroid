package pl.gdziejaide.gjiandroid;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import pl.gdziejaide.gjiandroid.api.GUI;
import pl.gdziejaide.gjiandroid.gui.MusicPlayer;
import pl.gdziejaide.gjiandroid.map.Compass;
import pl.gdziejaide.gjiandroid.map.GPS;
import pl.gdziejaide.gjiandroid.map.MyMapView;
import pl.gdziejaide.gjiandroid.map.ZonesOverlay;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.readers.XMLReader;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import com.google.android.maps.MapActivity;
import com.google.android.maps.Overlay;

public class ShowMapActivity extends MapActivity {
	GPS gps;
	Compass compass;
	Inventory inventory;
	public int rotation;
	MyMapView myMapView;
	String scenario_xml;
	int ii = 0;
	int xml_id;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showmap);

		xml_id=ObjectsContainer.getXmlId();
		
		loadScenario(xml_id);

		myMapView = (MyMapView) findViewById(R.id.mapview);
		myMapView.setBuiltInZoomControls(true);
		gps = new GPS(this, ObjectsContainer.getZones());
		gps.enable();
		compass = new Compass(this);
		compass.enable();
		inventory = new Inventory();

		List<Overlay> mapOverlays = myMapView.getOverlays();
		ZonesOverlay polygon = new ZonesOverlay(this, ObjectsContainer.getZones());
		mapOverlays.clear();
		mapOverlays.add(polygon);
		myMapView.invalidate();
		GUI.setContext(this);

		new Thread(new Runnable() {
			@Override
			public void run() {
				ObjectsContainer.getScenario().event("onBegin");
			}
		}).start();

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	public void refreshMap() {
		gps.markCurrentLocation();
		myMapView.invalidate();
	}

	@Override
	protected void onPause() {
		super.onPause();
		compass.disable();
		gps.disable();
	}

	@Override
	protected void onResume() {
		super.onResume();
		GUI.setContext(this);
		compass.enable();
		gps.enable();
	}

	private void loadScenario(int xml_id) {
		File SDCardRoot = Environment.getExternalStorageDirectory();
		String xmlPath = SDCardRoot.getAbsolutePath() + "/gji/scenario/scenario"+xml_id+"/scenario.xml";
		String xml = null;
		try {
			InputStream in_s = new BufferedInputStream(new FileInputStream(xmlPath));
			byte[] b = new byte[in_s.available()];
			in_s.read(b);
			xml = new String(b);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		XMLReader reader = new XMLReader(xml);
		Map<Integer, GJIObject> objects = null;
		try {
			objects = reader.createObjects();
		} catch (Exception e) {
			System.err.println("No coś poszło nie tak i z zabawy nici");
			e.printStackTrace();
			return;
		}
		ObjectsContainer.clearAll();
		ObjectsContainer.addObjects(objects);
		ObjectsContainer.setXmlId(xml_id);
	}
	public void centerMap(View view) {
		myMapView.centerMap();
	}
	public void showZoneList(View view) {
		Intent intent = new Intent(this, ZoneListActivity.class);
		startActivity(intent);
	}
	public void showNPCList(View view) {
		Intent intent = new Intent(this, CharacterListActivity.class);
		startActivity(intent);
	}
	public void showItemList(View view) {
		Intent intent = new Intent(this, ItemListActivity.class);
		startActivity(intent);
	}
	public void showQuestList(View view) {
		Intent intent = new Intent(this, QuestListActivity.class);
		startActivity(intent);
	}
	public void showInventoryList(View view) {
		Intent intent = new Intent(this, InventoryListActivity.class);
		startActivity(intent);
	}
	
	public void stopMusic(View view) {
		MusicPlayer.stop();
	}
	
	public void mockAgain(View view) {
		switch (ii) {
			case 0 :
				//setMockLocation(52.387653, 21.872585, 50);
				setMockLocation(52.403514,16.948979, 50);
				break;
			case 1 :
				//setMockLocation(52.419958, 16.93589, 50);
				setMockLocation(52.404174,16.949602, 50);
				break;
			case 2 :
				//setMockLocation(52.408334, 16.933735, 50);
				setMockLocation(52.400768,16.950898,50);
				break;
			case 3 :
				//setMockLocation(52.487653, 21.772585, 50);
				setMockLocation(52.402515,16.950702,50);
				break;
//			case 4 :
//				setMockLocation(52.236128, 18.278685, 50);
//				break;
//			case 5 :
//				setMockLocation(52.421247, 16.93731, 50);
//				break;
//			case 6 :
//				setMockLocation(52.387653, 21.872585, 50);
//				break;
		}
		ii = (ii + 1) % 4;
		centerMap(view);
	}

	private void setMockLocation(double latitude, double longitude, float accuracy) {
		LocationManager lm = gps.locationManager;
		lm.addTestProvider(LocationManager.GPS_PROVIDER, "requiresNetwork" == "", "requiresSatellite" == "", "requiresCell" == "",
				"hasMonetaryCost" == "", "supportsAltitude" == "", "supportsSpeed" == "", "supportsBearing" == "",
				android.location.Criteria.POWER_LOW, android.location.Criteria.ACCURACY_FINE);

		Location newLocation = new Location(LocationManager.GPS_PROVIDER);
		newLocation.setLatitude(latitude);
		newLocation.setLongitude(longitude);
		newLocation.setAccuracy(accuracy);
		newLocation.setTime(System.currentTimeMillis());

		lm.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);
		lm.setTestProviderStatus(LocationManager.GPS_PROVIDER, LocationProvider.AVAILABLE, null, System.currentTimeMillis());
		lm.setTestProviderLocation(LocationManager.GPS_PROVIDER, newLocation);

		Log.d("zeq", "setnąłemMockLocation");
	}

}
