package pl.gdziejaide.gjiandroid;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.map.GPS;
import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Zone;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class InventoryListActivity extends MyActivity {

	List<Item> items = new ArrayList<Item>();

	private class ItemListAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		public ItemListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return items.get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;

			if (convertView == null)
				vi = inflater.inflate(R.layout.item_list_row, null);
			TextView tv1 = (TextView) vi.findViewById(R.id.textView1);

			Item item = (Item) getItem(position);

			tv1.setText(item.getName());

			return vi;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_inventory_list);
		setDimensions();

		ListView lv = (ListView) findViewById(R.id.listView1);
		for (Item item : ObjectsContainer.getItems()) {
			if (Inventory.inventory.list().contains(item.getId())) {
				items.add(item);
			}
		}

		ItemListAdapter itemListAdapter = new ItemListAdapter(this);

		lv.setAdapter(itemListAdapter);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
				showInventoryInfo(items.get(arg2).getId());
			}
		});
	}

	public void showInventoryInfo(int itemId) {
		Intent intent = new Intent(this, InventoryInfoActivity.class);
		intent.putExtra("ITEM_ID", itemId);
		startActivity(intent);
		finish();
	}

	public void finishHim(View view) {
		finish();
	}
}
