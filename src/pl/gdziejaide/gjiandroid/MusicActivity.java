package pl.gdziejaide.gjiandroid;

import java.io.IOException;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

public class MusicActivity extends Activity {
	//MediaPlayer mp;
	VideoView vv; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_music);
		//Uri myUri= "gji/";
		/*mp=new MediaPlayer();
		mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
		try {
			mp.setSurface(findViewById(R.id.videoView1));
			mp.setDataSource("/sdcard/MP.mp4");
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
		vv = (VideoView)findViewById(R.id.videoView1);
		vv.setZOrderOnTop(true);
		vv.setVideoPath("/sdcard/MP.mp4");
		
	}

	@SuppressWarnings("deprecation")
	public void setDimensions() {
		WindowManager.LayoutParams params = getWindow().getAttributes();
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		switch (metrics.densityDpi) {
			case DisplayMetrics.DENSITY_HIGH : {
				height -= 48;
				break;
			}
			case DisplayMetrics.DENSITY_MEDIUM : {
				height -= 32;
				break;
			}
			case DisplayMetrics.DENSITY_LOW : {
				height -= 24;
				break;
			}
		}

		//params.width = (int) (0.70 * width);
		//params.height = (int) (0.70 * height);
		//this.getWindow().setAttributes(params);
	}
	public void stopMusic(View view) {
		//mp.stop();
		if(vv.isPlaying()) vv.pause();
	}
	public void playMusic(View view) {
		
		
/*		try {
			mp.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mp.start();*/
		if(!vv.isPlaying()) vv.start();
	}
}
