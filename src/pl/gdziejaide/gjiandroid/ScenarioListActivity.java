package pl.gdziejaide.gjiandroid;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.gdziejaide.gjiandroid.http.FileResponseCallback;
import pl.gdziejaide.gjiandroid.http.ServerAPI;
import pl.gdziejaide.gjiandroid.http.StringResponseCallback;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class ScenarioListActivity extends Activity {
	//private ArrayList<ScenarioHeader> scenarioHeaderList;
	private ArrayList<ScenarioHeader> publicScenarioHeaderList;
	private ArrayList<ScenarioHeader> userScenarioHeaderList;
	private class ScenarioListAdapter extends BaseAdapter {

		private ArrayList<ScenarioHeader> scenarioHeaderList;
		private LayoutInflater inflater;
		public ScenarioListAdapter(Activity activity, ArrayList<ScenarioHeader> list) {
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			scenarioHeaderList = list;
		}
		@Override
		public int getCount() {
			return scenarioHeaderList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;

			if (convertView == null)
				vi = inflater.inflate(R.layout.scenario_list_row, null);
			TextView tv1 = (TextView) vi.findViewById(R.id.textView1);
			TextView tv2 = (TextView) vi.findViewById(R.id.textView2);
			TextView tv3 = (TextView) vi.findViewById(R.id.textView3);
			TextView tv4 = (TextView) vi.findViewById(R.id.textView4);
			Log.d("scen", "position = " + position);
			ScenarioHeader scenario = scenarioHeaderList.get(position);

			tv1.setText(scenario.getName());
			tv2.setText(scenario.getDescription());
			if(tv2.getText().toString().trim().length()==0) tv2.setVisibility(View.GONE);//tv2.setText("Brak opisu.");
			tv3.setText(scenario.getAuthor());
			tv4.setText(scenario.getCreated());

			ImageView img = (ImageView) vi.findViewById(R.id.imageView1);
			Bitmap bm = scenarioHeaderList.get(position).getBitmap();
			if (bm != null)
				img.setImageBitmap(bm);

			return vi;

		}

	}
	
	
	
	
	public class SeparatedListAdapter extends BaseAdapter {  
	      
	    public final Map<String,Adapter> sections = new LinkedHashMap<String,Adapter>();  
	    public final ArrayAdapter<String> headers;  
	    public final static int TYPE_SECTION_HEADER = 0;  
	      
	    public SeparatedListAdapter(Context context) {  
	        headers = new ArrayAdapter<String>(context, R.layout.activity_scenario_list_header);  
	    }  
	      
	    public void addSection(String section, Adapter adapter) {  
	        this.headers.add(section);  
	        this.sections.put(section, adapter);  
	    }  
	      
	    public Object getItem(int position) {  
	        for(Object section : this.sections.keySet()) {  
	            Adapter adapter = sections.get(section);  
	            int size = adapter.getCount() + 1;  
	              
	            // check if position inside this section   
	            if(position == 0) return section;  
	            if(position < size) return adapter.getItem(position - 1);  
	  
	            // otherwise jump into next section  
	            position -= size;  
	        }  
	        return null;  
	    }  
	  
	    public int getCount() {  
	        // total together all sections, plus one for each section header  
	        int total = 0;  
	        for(Adapter adapter : this.sections.values())  
	            total += adapter.getCount() + 1;  
	        return total;  
	    }  
	  
	    public int getViewTypeCount() {  
	        // assume that headers count as one, then total all sections  
	        int total = 1;  
	        for(Adapter adapter : this.sections.values())  
	            total += adapter.getViewTypeCount();  
	        return total;  
	    }  
	      
	    public int getItemViewType(int position) {  
	        int type = 1;  
	        for(Object section : this.sections.keySet()) {  
	            Adapter adapter = sections.get(section);  
	            int size = adapter.getCount() + 1;  
	              
	            // check if position inside this section   
	            if(position == 0) return TYPE_SECTION_HEADER;  
	            if(position < size) return type + adapter.getItemViewType(position - 1);  
	  
	            // otherwise jump into next section  
	            position -= size;  
	            type += adapter.getViewTypeCount();  
	        }  
	        return -1;  
	    }  
	      
	    public boolean areAllItemsSelectable() {  
	        return false;  
	    }  
	  
	    public boolean isEnabled(int position) {  
	        return (getItemViewType(position) != TYPE_SECTION_HEADER);  
	    }  
	      
	    @Override  
	    public View getView(int position, View convertView, ViewGroup parent) {  
	        int sectionnum = 0;  
	        for(Object section : this.sections.keySet()) {  
	            Adapter adapter = sections.get(section);  
	            int size = adapter.getCount() + 1;  
	              
	            // check if position inside this section   
	            if(position == 0) return headers.getView(sectionnum, convertView, parent);  
	            if(position < size) return adapter.getView(position - 1, convertView, parent);  
	  
	            // otherwise jump into next section  
	            position -= size;  
	            sectionnum++;  
	        }  
	        return null;  
	    }  
	  
	    @Override  
	    public long getItemId(int position) {  
	        return position;  
	    }  
	  
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scenario_list);
		ObjectsContainer.setXmlId(1);
		//scenarioHeaderList = new ArrayList<ScenarioHeader>();
		//addSampleScenarios();
		
		publicScenarioHeaderList=new ArrayList<ScenarioHeader>();
		userScenarioHeaderList=new ArrayList<ScenarioHeader>();
		
		downloadScenarioList();
		downloadUserScenarioList();
		
		//refreshList();

	}

	private void downloadScenarioList() {
		ServerAPI.getPublicList(new StringResponseCallback() {

			@Override
			public void stringCallback(String response) {
				// ArrayList<ScenarioHeader> list = ServerAPI
				publicScenarioHeaderList = ServerAPI.XmlListToScenarioHeaderList(response);
				for (ScenarioHeader scenarioHeader : publicScenarioHeaderList) {
					System.out.println(scenarioHeader.getXmlId() + " " + scenarioHeader.getName() + " " + scenarioHeader.getDescription() + " "
							+ scenarioHeader.getCreated() + " " + scenarioHeader.getAuthor());
				}
				for (int i = 0; i < publicScenarioHeaderList.size(); i++) {
					// ListView list = (ListView)findViewById(R.id.listView1);
					// ScenarioListAdapter adapter = (ScenarioListAdapter)
					// list.getAdapter();
					downloadSplash(publicScenarioHeaderList.get(i).getXmlId(), publicScenarioHeaderList, i);
				}
				refreshList();
			}
		});
	}
	
	private void downloadUserScenarioList() {
		ServerAPI.getUserScenarios(new StringResponseCallback() {

			@Override
			public void stringCallback(String response) {
				// ArrayList<ScenarioHeader> list = ServerAPI
				userScenarioHeaderList = ServerAPI.XmlListToScenarioHeaderList(response);
				for (ScenarioHeader scenarioHeader : userScenarioHeaderList) {
					System.out.println(scenarioHeader.getXmlId() + " " + scenarioHeader.getName() + " " + scenarioHeader.getDescription() + " "
							+ scenarioHeader.getCreated() + " " + scenarioHeader.getAuthor());
				}
				for (int i = 0; i < userScenarioHeaderList.size(); i++) {
					// ListView list = (ListView)findViewById(R.id.listView1);
					// ScenarioListAdapter adapter = (ScenarioListAdapter)
					// list.getAdapter();
					downloadSplash(userScenarioHeaderList.get(i).getXmlId(), userScenarioHeaderList, i);
				}
				refreshList();
			}
		});
	}

	private void downloadSplash(int id, final ArrayList<ScenarioHeader> list, final int position) {
		ServerAPI.getSplash(id, new FileResponseCallback() {

			@Override
			public void progressCallback(int size, int downloaded) {
//				System.out.println(size + " " + downloaded);
			}

			@Override
			public void fileCallback(File response) { // tu zwroci sciezke do
														// splash
				System.out.println("Koniec - plik: " + (response != null ? response.getAbsolutePath() : ""));
				// response.getAbsolutePath()
				if (response == null) return;
				Bitmap bm = BitmapFactory.decodeFile(response.getAbsolutePath());
				list.get(position).setBitmap(bm);
				ListView list = (ListView) findViewById(R.id.listView1);
				SeparatedListAdapter adapter = (SeparatedListAdapter) list.getAdapter();
				adapter.notifyDataSetChanged();

			}
		});

	}

	private void downloadScenario() {
		ServerAPI.getScenario(4, new FileResponseCallback() {

			@Override
			public void progressCallback(int size, int downloaded) {
//				System.out.println(size + " " + downloaded);
			}

			@Override
			public void fileCallback(File response) { // tu zwroci sciezke do
														// folderu ze
														// scenariuszem
				System.out.println("Koniec - folder: " + (response != null ? response.getAbsolutePath() : ""));
			}
		});
	}

	private void refreshList() {
		final Activity activity = this;
          
        // create our list and custom adapter  
        SeparatedListAdapter adapterM = new SeparatedListAdapter(this);  
        adapterM.addSection("Publiczne", new ScenarioListAdapter(this, publicScenarioHeaderList));  
        adapterM.addSection("Użytkownika", new ScenarioListAdapter(this, userScenarioHeaderList));  
		ListView list = (ListView) findViewById(R.id.listView1);
		list.setAdapter(adapterM);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				doSthClicked(activity, position, arg1);

			}

		});
	}

	private void doSthClicked(Activity activity, int position, View view) {
		 Intent intent = new Intent(activity, ShowMapActivity.class);
		 //intent.putExtra("xml_id", 1);
		 //publicScenarioHeaderList.get(position).getXmlId());
		 startActivity(intent);
		// downloadSplash(scenarioHeaderList.get(position).getXmlId(), view);
	}

	private void addSampleScenarios() {
		ScenarioHeader scen1 = new ScenarioHeader(R.raw.xmltest, "PoznanioKonin",
				"A taki tam testowy scenariusz, z paroma fajnymi, a nawet bardzo fajnymi żonami, lol..", "", "");
		ScenarioHeader scen2 = new ScenarioHeader(R.raw.silnia, "Silnia", "Test scenariusza z silnią. For fun and knowledge.", "", "");
		//scenarioHeaderList.add(scen1);
		//scenarioHeaderList.add(scen2);
		//scenarioHeaderList.add(scen2);
		//scenarioHeaderList.add(scen2);
	}
	public void finishHim(View view) {
		finish();
	}
}
