package pl.gdziejaide.gjiandroid;

import java.io.File;

import pl.gdziejaide.gjiandroid.api.GUI;
import pl.gdziejaide.gjiandroid.gui.Dialogs;
import pl.gdziejaide.gjiandroid.objects.Character;
import pl.gdziejaide.gjiandroid.objects.Media;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CharacterInfoActivity extends MyActivity {

	int characterId;
	Character character;
	int xml_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_character_info);
		setDimensions();

		Intent intent = getIntent();
		characterId = intent.getIntExtra("CHARACTER_ID", -1);
		character = (Character) ObjectsContainer.getById(characterId);
		xml_id=ObjectsContainer.getXmlId();
		
		
		if (character.icon != -1) {			
			Media image = ObjectsContainer.getImage(character.icon);
			ImageView iv = (ImageView) findViewById(R.id.imageView1);
			File SDCardRoot = Environment.getExternalStorageDirectory();
			String bmPath = SDCardRoot.getAbsolutePath() + "/gji/scenario/scenario" + xml_id + "/" + image.getPath();
			Bitmap bm = BitmapFactory.decodeFile(bmPath);
			iv.setImageBitmap(bm);
		}

		
		completeInfo();
		GUI.setContext(this);
	}

	public void completeInfo() {
		TextView tv = (TextView) findViewById(R.id.textView1);
		tv.setText(character.name);
		tv = (TextView) findViewById(R.id.textView2);
		tv.setText(character.description);
	}

	public void talkWithMe(View view) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				character.event("onDialog");
			}
		}).start();
	}

	public void finishHim(View view) {
		Intent intent = new Intent(this, CharacterListActivity.class);
		startActivity(intent);
		finish();
	}

	public void onBackPressed() {
		Intent intent = new Intent(this, CharacterListActivity.class);
		startActivity(intent);
		finish();
	}
}
