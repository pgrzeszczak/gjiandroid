package pl.gdziejaide.gjiandroid;

import android.graphics.Bitmap;

public class ScenarioHeader {
	private int xmlId;
	private String name;
	private String description;
	private String author;
	private String created;
	private Bitmap bitmap = null;
	public int getXmlId() {
		return xmlId;
	}
	public void setXmlId(int xmlId) {
		this.xmlId = xmlId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public ScenarioHeader(int xmlId, String name, String description, String author, String created) {
		super();
		this.xmlId = xmlId;
		this.name = name;
		this.description = description;
		this.author = author;
		this.created = created;
	}
	public void setBitmap(Bitmap bm) {
		// TODO Auto-generated method stub
		this.bitmap = bm;

	}
	public Bitmap getBitmap() {
		return bitmap;
		// TODO Auto-generated method stub

	}
}
