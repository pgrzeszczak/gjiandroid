package pl.gdziejaide.gjiandroid;

import java.io.File;

import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.Media;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Quest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class QuestInfoActivity extends MyActivity {
	int xml_id;
	int questId;
	Quest quest;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_quest_info);
		setDimensions();

		Intent intent = getIntent();
		questId = intent.getIntExtra("QUEST_ID", -1);
		quest = (Quest) ObjectsContainer.getById(questId);
		
		xml_id=ObjectsContainer.getXmlId();
		if (quest.icon != -1) {			
			Media image = ObjectsContainer.getImage(quest.icon);
			ImageView iv = (ImageView) findViewById(R.id.imageView1);
			File SDCardRoot = Environment.getExternalStorageDirectory();
			String bmPath = SDCardRoot.getAbsolutePath() + "/gji/scenario/scenario" + xml_id + "/" + image.getPath();
			Bitmap bm = BitmapFactory.decodeFile(bmPath);
			iv.setImageBitmap(bm);
		}

		completeInfo();
	}

	public void completeInfo() {
		TextView tv = (TextView) findViewById(R.id.textView1);
		tv.setText(quest.name);
		tv = (TextView) findViewById(R.id.textView2);
		tv.setText(quest.description);
	}
	public void finishHim(View view) {
		Intent intent = new Intent(this, QuestListActivity.class);
		startActivity(intent);
		finish();
	}

	public void doNothing(View view) {

	}

	public void onBackPressed() {
		Intent intent = new Intent(this, QuestListActivity.class);
		startActivity(intent);
		finish();
	}

}
