package pl.gdziejaide.gjiandroid;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.map.GPS;
import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Quest;
import pl.gdziejaide.gjiandroid.objects.Zone;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class QuestListActivity extends MyActivity {

	List<Quest> quests = new ArrayList<Quest>();

	private class QuestListAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		public QuestListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		@Override
		public int getCount() {
			return quests.size();
		}

		@Override
		public Object getItem(int position) {
			return quests.get(position);
		}

		@Override
		public long getItemId(int position) {
			return quests.get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;

			if (convertView == null)
				vi = inflater.inflate(R.layout.quest_list_row, null);
			TextView tv1 = (TextView) vi.findViewById(R.id.textView1);

			Quest quest = (Quest) getItem(position);

			tv1.setText(quest.getName());

			return vi;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_quest_list);
		setDimensions();

		for(Quest quest : ObjectsContainer.getQuests()) {
			if(quest.isVisible()) quests.add(quest);
		}
		//quests=ObjectsContainer.getQuests();
		

		QuestListAdapter adapter = new QuestListAdapter(this);

		ListView lv = (ListView) findViewById(R.id.listView1);
		lv.setAdapter(adapter);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
				showQuestInfo(quests.get(arg2).getId());
			}
		});
	}

	public void showQuestInfo(int itemId) {
		Intent intent = new Intent(this, QuestInfoActivity.class);
		intent.putExtra("QUEST_ID", itemId);
		startActivity(intent);
		finish();
	}

	public void finishHim(View view) {
		finish();
	}
}
