package pl.gdziejaide.gjiandroid;

import pl.gdziejaide.gjiandroid.http.ServerAPI;
import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("lol", "Otwieram okno!");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		TextView et = (TextView) findViewById(R.id.editText1);
		et.setText(ServerAPI.username);
		et = (TextView) findViewById(R.id.editText2);
		et.setText(ServerAPI.password);
		//setDimensions();

	}

	@SuppressWarnings("deprecation")
	public void setDimensions() {
		WindowManager.LayoutParams params = getWindow().getAttributes();
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		switch (metrics.densityDpi) {
			case DisplayMetrics.DENSITY_HIGH : {
				height -= 48;
				break;
			}
			case DisplayMetrics.DENSITY_MEDIUM : {
				height -= 32;
				break;
			}
			case DisplayMetrics.DENSITY_LOW : {
				height -= 24;
				break;
			}
		}

		//params.width = (int) (0.70 * width);
		//params.height = (int) (0.70 * height);
		//this.getWindow().setAttributes(params);
	}
	public void logMeInClicked(View view) {
		TextView et = (TextView) findViewById(R.id.editText1);
		String login=et.getText().toString();
		et = (TextView) findViewById(R.id.editText2);
		String pass=et.getText().toString();
		Log.d("lol", "Teeeest!");
		Log.d("lol", login.toString());
		Log.d("lol", pass.toString());
		if(login!="" && pass!="") ServerAPI.setCredentials(login, pass);
		
		finish();
	}
	public void backClicked(View view) {
		finish();
	}

}
