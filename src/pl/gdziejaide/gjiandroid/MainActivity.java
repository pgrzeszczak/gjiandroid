package pl.gdziejaide.gjiandroid;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pl.gdziejaide.gjiandroid.api.GUI;
import pl.gdziejaide.gjiandroid.http.FileResponseCallback;
import pl.gdziejaide.gjiandroid.http.ServerAPI;
import pl.gdziejaide.gjiandroid.http.StringResponseCallback;
import pl.gdziejaide.gjiandroid.map.Compass;
import pl.gdziejaide.gjiandroid.objects.GJIObject;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.readers.XMLReader;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	private Compass compass;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initializeServerAPI();
//		Przyklad pobierania scenariusza
//		ServerAPI.getScenario(2, new FileResponseCallback() {
//			
//			@Override
//			public void progressCallback(int size, int downloaded) {
//				System.out.println(size + " " + downloaded);
//			}
//			
//			@Override
//			public void fileCallback(File response) { //tu zwroci sciezke do folderu ze scenariuszem
//				System.out.println("Koniec - folder: " + (response != null ? response.getAbsolutePath() : ""));
//			}
//		});
//		Przyklad pobierania scenariusza z hasłem
//		ServerAPI.getScenario(3, "lol", new FileResponseCallback() {
//			
//			@Override
//			public void progressCallback(int size, int downloaded) {
//				System.out.println(size + " " + downloaded);
//			}
//			
//			@Override
//			public void fileCallback(File response) { //tu zwroci sciezke do folderu ze scenariuszem
//				System.out.println("Koniec - folder: " + (response != null ? response.getAbsolutePath() : ""));
//			}
//		});
//		Przyklad pobierania splash
//		ServerAPI.getSplash(4, new FileResponseCallback() {
//			
//			@Override
//			public void progressCallback(int size, int downloaded) {
//				System.out.println(size + " " + downloaded);
//			}
//			
//			@Override
//			public void fileCallback(File response) { //tu zwroci sciezke do splash
//				System.out.println("Koniec - plik: " + (response != null ? response.getAbsolutePath() : ""));
//			}
//		});
//		Bez parametrów - bierz 10 najnowszych scenariuszy
//		ServerAPI.setCredentials("test", "test"); //ustawianie użytkownika
//		ServerAPI.clearCredentials(); //usuwanie użytkownika
//		ServerAPI.getPublicList(new StringResponseCallback() {
//
//			@Override
//			public void stringCallback(String response) {
//				ArrayList<ScenarioHeader> list = ServerAPI
//						.XmlListToScenarioHeaderList(response);
//				for (ScenarioHeader scenarioHeader : list) {
//					System.out.println(scenarioHeader.getXmlId() + " "
//							+ scenarioHeader.getName() + " "
//							+ scenarioHeader.getDescription() + " "
//							+ scenarioHeader.getCreated() + " "
//							+ scenarioHeader.getAuthor());
//				}
//			}
//		});
//		Z parametrami - jaki offset i ile wziac
//		ServerAPI.getPublicList(new StringResponseCallback() {
//			
//			@Override
//			public void stringCallback(String response) {
//				System.out.println(response);
//			}
//		}, 1, 3);
		setContentView(R.layout.activity_main);
		GUI.setContext(this);
		// compass=new Compass(this, null);

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		GUI.setContext(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	public void openMapActivity(View view) {
		Intent intent = new Intent(this, ShowMapActivity.class);
		intent.putExtra("xml_id", R.raw.xmltest);
		startActivity(intent);
	}

	public void openScenarioListActivity(View view) {
		Intent intent = new Intent(this, ScenarioListActivity.class);
		startActivity(intent);
	}	
	
	public void playMusic(View view) {
		Intent intent = new Intent(this, MusicActivity.class);
		startActivity(intent);
		
		//openMapActivity(view);
	}
	
	public void showLoginActivity(View view) {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
	}
	
	public void initializeServerAPI() {
		Map<String, String> links = new HashMap<String, String>();
		links.put("server_url", getString(R.string.server_url));
		links.put("server_url_publiclist", getString(R.string.server_url_publiclist));
		links.put("server_url_userscenarios", getString(R.string.server_url_userscenarios));
		links.put("server_url_getsplash", getString(R.string.server_url_getsplash));
		links.put("server_url_getscenario", getString(R.string.server_url_getscenario));
		ServerAPI.addLinks(links);
	}

	public void testChoice() {
		final String[] params = {"Tityl", "Choice uno", "Coice kwatro", "Choice tricho"};
		final int result = GUI.choice(params);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				TextView tv = (TextView) findViewById(R.id.textView1);
				tv.setText("Wybrana opcja: " + Integer.toString(result) + " - " + params[result + 1]);
			}
		});
	}
	public void testDialog() {
		final String result = GUI.dialog("Tityl", "Messydż, napisz coś, tej.");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				TextView tv = (TextView) findViewById(R.id.textView1);
				tv.setText("Napisałeś: " + result);
			}
		});
	}
	public void testMessage() {
		GUI.message("titl", "messydż");
	}
	public void testWarning() {
		GUI.warning("titl", "messydż");
	}
	public void testConfirm() {
		final boolean czyzby = GUI.confirm("tityl", "Czy aby na pewno?!");
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (czyzby) {
					TextView tv = (TextView) findViewById(R.id.textView1);
					tv.setText("Yes we can!");
				} else {
					TextView tv = (TextView) findViewById(R.id.textView1);
					tv.setText("No, we can't. :(");
				}
			}
		});
	}

	public void guiTest(View view) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				testDialog();
			}
		}).start();
	}
	public void testSilnia(View view) {
		String xml = null;
		try {
			InputStream in_s = getResources().openRawResource(R.raw.silnia);
			byte[] b = new byte[in_s.available()];
			in_s.read(b);
			xml = new String(b);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		XMLReader reader = new XMLReader(xml);
		Map<Integer, GJIObject> objects = null;
		try {
			objects = reader.createObjects();
		} catch (Exception e) {
			System.err.println("No coś poszło nie tak i z zabawy nici");
			e.printStackTrace();
			return;
		}
		ObjectsContainer.clearAll();
		ObjectsContainer.addObjects(objects);
		new Thread() {
			public void run() {
				try {
					ObjectsContainer.getScenario().event("onbegin");
				} catch (Exception e) {
					System.err.println("No coś poszło nie tak i z zabawy nici");
					e.printStackTrace();
					return;
				}
			}
		}.start();
	}
	public void xmlParseTest(View view) {
		String xml = null;
		try {
			InputStream in_s = getResources().openRawResource(R.raw.testtimer);
			byte[] b = new byte[in_s.available()];
			in_s.read(b);
			xml = new String(b);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		XMLReader reader = new XMLReader(xml);
		Map<Integer, GJIObject> objects = null;
		try {
			objects = reader.createObjects();
		} catch (Exception e) {
			System.err.println("No coś poszło nie tak i z zabawy nici");
			e.printStackTrace();
			return;
		}
		ObjectsContainer.clearAll();
		ObjectsContainer.addObjects(objects);
		new Thread() {
			public void run() {
				try {
					ObjectsContainer.getScenario().event("onbegin");
				} catch (Exception e) {
					System.err.println("No coś poszło nie tak i z zabawy nici");
					e.printStackTrace();
					return;
				}
			}
		}.start();
	}
}
