package pl.gdziejaide.gjiandroid.map;

import java.util.List;

import pl.gdziejaide.gjiandroid.objects.Zone;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class GPS {
	private static final int TWO_MINUTES = 1000 * 60 * 2;
	public static GPS gps;
	public LocationManager locationManager;
	static Location currLocation;
	static GeoPoint geoCurrLocation;
	private ZoneEventQueue zeq;
	public int currZone=-1;

	private Context mContext;
	// List<Zone> zoneList;

	public GPS(Context context, List<Zone> zoneList) {
		mContext = context;
		// this.zoneList = zoneList;
		GPS.gps = this;
		locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		zeq = new ZoneEventQueue(zoneList);

	}

	private final LocationListener listener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			updateCurrentLocation(location);
			zeq.addLocation(geoCurrLocation);
//			Log.d("zeq", "Dodalem lokacje do zeq.");
		}

		public void onProviderDisabled(String provider) {

		}

		public void onProviderEnabled(String provider) {

		}

		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

	};
	public void updateCurrentLocation(Location location) {
		if (true || isBetterLocation(location, currLocation)) {
			GeoPoint currentLocation = new GeoPoint((int) (location.getLatitude() * 1E6), (int) (location.getLongitude() * 1E6));
			MyMapView.mapa.updateCurrentLocationMarker(currentLocation, location.getProvider(), location.getAccuracy());
			geoCurrLocation = currentLocation;
//			System.out.println(currLocation!=null?((location.getLatitude()-currLocation.getLatitude()) + " " + (location.getLongitude()-currLocation.getLongitude())):"");
//			System.out.println(location.hasSpeed() + " " + location.getSpeed() + " " + (location.hasSpeed() && location.getSpeed() >= 0.2));
			if (location.hasSpeed() && location.getSpeed() >= 0.2) {
				MyMapView.mapa.getController().setCenter(geoCurrLocation);
			}
			currLocation = location;
		}
	}
	public static GeoPoint getCurrentLocation() {
		return geoCurrLocation;
	}

	public void markCurrentLocation() {
		if (currLocation != null)
			updateCurrentLocation(currLocation);
	}
	public void enable() {
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
		// locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
		// 1000, 1, listener);
	}
	public void disable() {
		locationManager.removeUpdates(listener);
	}

	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 * 
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The current Location fix, to which you want to compare the new
	 *            one
	 */
	protected boolean isBetterLocation(Location location, Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
			return true;
		}
		return false;
	}
	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}
}
