package pl.gdziejaide.gjiandroid.map;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import pl.gdziejaide.gjiandroid.objects.Zone;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Region;

import com.google.android.maps.GeoPoint;

public class ZoneEventQueue {
	public static ZoneEventQueue ZEQ;
	private List<Zone> zoneList;
	private ArrayList<Path> pathList = new ArrayList<Path>();
	private ArrayList<GeoPoint> locationList = new ArrayList<GeoPoint>();
	private int currZone = -1;
	private GeoPoint geoCurrLocation;
	public ZoneEventQueue(List<Zone> zoneList) {
		ZoneEventQueue.ZEQ = this;
		this.zoneList = zoneList;
		preparePaths();
		doMyJob();
	}
	private void preparePaths() {
		pathList.clear();
		for (Zone zone : zoneList) {
			Path path = new Path();
			Point firstPoint = new Point(zone.getPoints().get(0).getLatitude(), zone.getPoints().get(0).getLongitude());
			path.moveTo(firstPoint.x, firstPoint.y);
			for (int i = 1; i < zone.getPoints().size(); ++i) {
				Point nextPoint = new Point(zone.getPoints().get(i).getLatitude(), zone.getPoints().get(i).getLongitude());
				path.lineTo(nextPoint.x, nextPoint.y);
			}
			path.lineTo(firstPoint.x, firstPoint.y);
			path.setLastPoint(firstPoint.x, firstPoint.y);
			pathList.add(path);
		}
	}
	public void addLocation(GeoPoint location) {
		locationList.add(location);
	}
	private void doMyJob() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					if (locationList.isEmpty()) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						geoCurrLocation = locationList.get(0);
						locationList.remove(0);
						int i = 0;
						boolean onLeaveFired = false;
						int newCurrZone = -1;
						for (Path path : pathList) {
							RectF rectF = new RectF();
							path.computeBounds(rectF, true);
							RectF rectF2 = new RectF(rectF);
							int offX = (int) rectF.left, offY = (int) rectF.top;
							rectF2.offsetTo(0, 0);
							Region region = new Region();
							Path newPath = new Path(path);
							newPath.offset(-offX, -offY);
							region.setPath(newPath, new Region((int) rectF2.left, (int) rectF2.top, (int) rectF2.right, (int) rectF2.bottom));

							Point point = new Point(geoCurrLocation.getLatitudeE6(), geoCurrLocation.getLongitudeE6());
							point.offset(-offX, -offY);

							// Jeśli jestem w środku danej strefy..
							if (region.contains(point.x, point.y)) {
								newCurrZone = i;
								GPS.gps.currZone = newCurrZone;
								// I jeśli jeszcze przed chwilą nie byłem w
								// śordku
								// danej
								// strefy..
								if (currZone != i) {
									// Ale byłem w środku jakiejs innej strefy..
									if (currZone != -1) {
										// To opusczam stara strefę..
										onLeaveFired = true;
										if (ObjectsContainer.getZones().get(currZone).isVisible())
											ObjectsContainer.getZones().get(currZone).event("onleave");

									}
									// I wchodzę do nowej..
									final int ii = i;
									if (ObjectsContainer.getZones().get(ii).isVisible())
										ObjectsContainer.getZones().get(ii).event("onenter");
								}
							}
							i++;
						}

						// Jeśli natomiast nie wszedłem do nowej strefy ale
						// opuściłem
						// starą..
						if (currZone != -1 && currZone != newCurrZone && !onLeaveFired) {
							if (ObjectsContainer.getZones().get(currZone).isVisible())
								ObjectsContainer.getZones().get(currZone).event("onleave");
						}
						currZone = newCurrZone;
						GPS.gps.currZone=newCurrZone;
					}
				}
			}
		}).start();

	}
}
