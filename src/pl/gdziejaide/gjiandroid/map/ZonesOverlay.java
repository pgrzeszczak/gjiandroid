package pl.gdziejaide.gjiandroid.map;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.objects.Zone;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class ZonesOverlay extends Overlay {
	Context mContext;
	List<Zone> zoneList;
	ArrayList<Path> pathList = new ArrayList<Path>();

	public void showDialog(String title, String message) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.show();
	}
	@Override
	public boolean onTap(GeoPoint geoPoint, MapView mapView) {
		int i = 0;
		for (Path path : pathList) {
			RectF rectF = new RectF();
			path.computeBounds(rectF, true);
			Region region = new Region();
			if (region.setPath(path, new Region((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom)))
				Log.d("np", "ZOO: Stworzyłem region.");

			Point point = new Point();
			mapView.getProjection().toPixels(geoPoint, point);

			if (region.contains(point.x, point.y) && zoneList.get(i).visible == true) {
				showDialog(zoneList.get(i).getName(), zoneList.get(i).getDescription());
			}
			i++;
		}

		return super.onTap(geoPoint, mapView);
	}
	ArrayList<GeoPoint> geoPoints;
	public ZonesOverlay(Context context, List<Zone> zoneList) {
		this.zoneList = zoneList;
		mContext = context;

	}
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		Paint polygonPaint = new Paint();
		polygonPaint.setColor(Color.RED);
		polygonPaint.setAlpha(64);
		polygonPaint.setStyle(Paint.Style.FILL_AND_STROKE);

		Paint edgePaint = new Paint();
		edgePaint.setColor(Color.BLACK);
		edgePaint.setAlpha(128);
		edgePaint.setStyle(Paint.Style.STROKE);

		pathList.clear();
		for (Zone zone : zoneList) {
			Path path = new Path();
			Point firstPoint = new Point();
			mapView.getProjection().toPixels(zone.getPoints().get(0).getGeo(), firstPoint);
			path.moveTo(firstPoint.x, firstPoint.y);
			for (int i = 1; i < zone.getPoints().size(); ++i) {
				Point nextPoint = new Point();
				mapView.getProjection().toPixels(zone.getPoints().get(i).getGeo(), nextPoint);
				path.lineTo(nextPoint.x, nextPoint.y);
			}
			path.lineTo(firstPoint.x, firstPoint.y);
			path.setLastPoint(firstPoint.x, firstPoint.y);
			pathList.add(path);
			if (zone.visible == true) {
				canvas.drawPath(path, polygonPaint);
				canvas.drawPath(path, edgePaint);
			}

		}

		super.draw(canvas, mapView, shadow);

	}
}
