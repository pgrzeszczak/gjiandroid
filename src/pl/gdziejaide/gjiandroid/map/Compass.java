package pl.gdziejaide.gjiandroid.map;

import pl.gdziejaide.gjiandroid.ShowMapActivity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Compass {
	public static Compass compass;
	private SensorManager mSensorManager;
	private Sensor msensor;
	private Sensor gsensor;
	private Context mContext;
	private float[] mGData = new float[3];
	private float[] mMData = new float[3];
	private float[] mR = new float[16];
	private float[] mI = new float[16];
	private float[] mOrientation = new float[3];
	private int mCount = 0;

	public Compass(Context context) {
		this.mContext = context;
		Compass.compass = this;
		mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
		gsensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		msensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	}

	private final SensorEventListener mListener = new SensorEventListener() {

		@Override
		public void onSensorChanged(SensorEvent event) {
			switch (event.sensor.getType()) {
				case Sensor.TYPE_ACCELEROMETER : {
					for (int i = 0; i < 3; i++)
						mGData[i] = event.values[i];
					break;
				}
				case Sensor.TYPE_MAGNETIC_FIELD : {
					for (int i = 0; i < 3; i++)
						mMData[i] = event.values[i];
					break;
				}
			}
			mCount++;
			SensorManager.getRotationMatrix(mR, mI, mGData, mMData);
			SensorManager.getOrientation(mR, mOrientation);
			final float rad2deg = (float) (180.0f / Math.PI);
			// Log.d("Compass", "yaw: " + (int) (mOrientation[0] * rad2deg) +
			// "  pitch: " + (int) (mOrientation[1] * rad2deg) + "  roll: "
			// + (int) (mOrientation[2] * rad2deg) + "  incl: " + (int) (incl *
			// rad2deg));
			if (mCount == 10) {
				mCount = 0;
				((ShowMapActivity) mContext).rotation = (int) (mOrientation[0] * rad2deg);
				((ShowMapActivity) mContext).refreshMap();
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {

		}
	};

	public void enable() {
		mSensorManager.registerListener(mListener, gsensor, SensorManager.SENSOR_DELAY_GAME);
		mSensorManager.registerListener(mListener, msensor, SensorManager.SENSOR_DELAY_GAME);
	}

	public void disable() {
		mSensorManager.unregisterListener(mListener);
	}

}
