package pl.gdziejaide.gjiandroid.map;

import java.util.List;

import pl.gdziejaide.gjiandroid.R;
import pl.gdziejaide.gjiandroid.ShowMapActivity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class MyMapView extends MapView {

	public static MyMapView mapa;
	private MyItemizedOverlay myLocationOverlay;
	private Drawable myLocationDrawable;

	
	public MyMapView(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		mapa = this;
		myLocationDrawable = getResources().getDrawable(R.drawable.arrow);
		myLocationOverlay = new MyItemizedOverlay(myLocationDrawable, getContext());
		
	}
	public void centerMap() {
		if (GPS.getCurrentLocation() != null)
			this.getController().animateTo(GPS.getCurrentLocation());
	}
	public void animateToPoint(GeoPoint p) {
		this.getController().animateTo(p);
	}
	public void updateCurrentLocationMarker(GeoPoint geoPoint, String provider, float accuracy) {
		List<Overlay> mapOverlays = getOverlays();
		mapOverlays.remove(myLocationOverlay);
		myLocationOverlay.clear();

		Bitmap bmpOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);
		Bitmap bmResult = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas tempCanvas = new Canvas(bmResult);
		tempCanvas.rotate(((ShowMapActivity) getContext()).rotation, bmpOriginal.getWidth() / 2, bmpOriginal.getHeight() / 2);
		tempCanvas.drawBitmap(bmpOriginal, 0, 0, null);
		BitmapDrawable newDrawable = new BitmapDrawable(bmResult);
		myLocationOverlay = new MyItemizedOverlay(newDrawable, getContext());

		OverlayItem overlayitem = new OverlayItem(geoPoint, provider, "Acc: "+accuracy);
		myLocationOverlay.addOverlay(overlayitem);
		mapOverlays.add(myLocationOverlay);
		invalidate();
	}
}
