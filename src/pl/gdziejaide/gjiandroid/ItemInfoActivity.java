package pl.gdziejaide.gjiandroid;

import java.io.File;

import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.Media;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemInfoActivity extends MyActivity {

	int itemId;
	Item item;
	int xml_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_item_info);
		setDimensions();

		xml_id=ObjectsContainer.getXmlId();
		
		Intent intent = getIntent();
		itemId = intent.getIntExtra("ITEM_ID", -1);
		item = (Item) ObjectsContainer.getById(itemId);
		System.out.println("xml_id"+xml_id);
		if (item.icon != -1) {			
			Media image = ObjectsContainer.getImage(item.icon);
			ImageView iv = (ImageView) findViewById(R.id.imageView1);
			File SDCardRoot = Environment.getExternalStorageDirectory();
			String bmPath = SDCardRoot.getAbsolutePath() + "/gji/scenario/scenario" + xml_id + "/" + image.getPath();
			Bitmap bm = BitmapFactory.decodeFile(bmPath);
			iv.setImageBitmap(bm);
		}
		completeInfo();
	}

	public void completeInfo() {
		TextView tv = (TextView) findViewById(R.id.textView1);
		tv.setText(item.name);
		tv = (TextView) findViewById(R.id.textView2);
		tv.setText(item.description);
	}
	public void finishHim(View view) {
		Intent intent = new Intent(this, ItemListActivity.class);
		startActivity(intent);
		finish();
	}

	public void onBackPressed() {
		Intent intent = new Intent(this, ItemListActivity.class);
		startActivity(intent);
		finish();
	}

	public void takeItem(View view) {
		Inventory.inventory.add(itemId);
		new Thread(new Runnable() {

			@Override
			public void run() {
				item.event("onFind");
			}
		}).start();
		finish();
	}

}
