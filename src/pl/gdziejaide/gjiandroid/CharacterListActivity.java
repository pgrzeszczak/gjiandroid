package pl.gdziejaide.gjiandroid;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.map.GPS;
import pl.gdziejaide.gjiandroid.objects.Character;
import pl.gdziejaide.gjiandroid.objects.Item;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CharacterListActivity extends MyActivity {

	List<Character> characters = new ArrayList<Character>();

	private class CharacterListAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		public CharacterListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		@Override
		public int getCount() {
			return characters.size();
		}

		@Override
		public Object getItem(int position) {
			return characters.get(position);
		}

		@Override
		public long getItemId(int position) {
			return characters.get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null)
				vi = inflater.inflate(R.layout.character_list_row, null);
			Character character = (Character) getItem(position);

			TextView tv1 = (TextView) vi.findViewById(R.id.textView1);
			tv1.setText(character.getName());

			return vi;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_npclist);
		setDimensions();

		ListView lv = (ListView) findViewById(R.id.listView1);
		ArrayList<String> characterNames = new ArrayList<String>();
		for (Character character : ObjectsContainer.getCharacters()) {
			if (GPS.gps.currZone != -1 && character.zone == ObjectsContainer.getZones().get(GPS.gps.currZone).getId() && character.isVisible()) {
				characterNames.add(character.name);
				characters.add(character);
			}
		}

		CharacterListAdapter adapter = new CharacterListAdapter(this);

		lv.setAdapter(adapter);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
				showCharacterInfo(characters.get(arg2).getId());
			}
		});
	}

	public void showCharacterInfo(int characterId) {
		Intent intent = new Intent(this, CharacterInfoActivity.class);
		intent.putExtra("CHARACTER_ID", characterId);
		startActivity(intent);
		finish();
	}

	public void finishHim(View view) {
		finish();
	}

}
