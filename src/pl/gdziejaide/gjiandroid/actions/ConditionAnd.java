package pl.gdziejaide.gjiandroid.actions;

import java.util.ArrayList;
import java.util.List;

public class ConditionAnd implements ConditionInterface {
	List<ConditionInterface> conditions;
	
	public ConditionAnd() {
		conditions = new ArrayList<ConditionInterface>();
	}

	public void addCondition(ConditionInterface condition) {
		conditions.add(condition);
	}
	
	public void addConditions(List<ConditionInterface> conditions) {
		this.conditions.addAll(conditions);
	}
	
	public boolean evaluate() {
		if (conditions.size() == 0) {
			return false;
		}
		for (ConditionInterface condition: conditions) {
			if (condition.evaluate() == false) {
				return false;
			}
		}
		return true;
	}

}
