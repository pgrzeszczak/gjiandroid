package pl.gdziejaide.gjiandroid.actions;

import pl.gdziejaide.gjiandroid.api.UTILS;
import pl.gdziejaide.gjiandroid.enums.CharacterState;
import pl.gdziejaide.gjiandroid.enums.ItemState;
import pl.gdziejaide.gjiandroid.enums.PackageType;
import pl.gdziejaide.gjiandroid.enums.QuestState;
import pl.gdziejaide.gjiandroid.enums.ZoneState;

public class CallFunctionActionUtils extends CallFunctionAction {

	public CallFunctionActionUtils(String function, String target) {
		super(PackageType.GUI, function, target);
	}

	public void process() {
		Object result = null;
		if (function.equalsIgnoreCase("changeZoneState") && arguments.size() == 2) {
			UTILS.changeZoneState((Integer)arguments.get(0).getArgument(), ZoneState.getFromString((String)arguments.get(1).getArgument()));
		} else if (function.equalsIgnoreCase("changeCharacterState") && arguments.size() == 2) {
			UTILS.changeCharacterState((Integer)arguments.get(0).getArgument(), CharacterState.getFromString((String)arguments.get(1).getArgument()));
		} else if (function.equalsIgnoreCase("changeItemState") && arguments.size() == 2) {
			UTILS.changeItemState((Integer)arguments.get(0).getArgument(), ItemState.getFromString((String)arguments.get(1).getArgument()));
		} else if (function.equalsIgnoreCase("changeQuestState") && arguments.size() == 2) {
			UTILS.changeQuestState((Integer)arguments.get(0).getArgument(), QuestState.getFromString((String)arguments.get(1).getArgument()));
		} else if (function.equalsIgnoreCase("changeZoneVisibility") && arguments.size() == 2) {
			UTILS.changeZoneVisibility((Integer)arguments.get(0).getArgument(), (Boolean)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("changeCharacterVisibility") && arguments.size() == 2) {
			UTILS.changeCharacterVisibility((Integer)arguments.get(0).getArgument(), (Boolean)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("changeItemVisibility") && arguments.size() == 2) {
			UTILS.changeItemVisibility((Integer)arguments.get(0).getArgument(), (Boolean)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("changeQuestVisibility") && arguments.size() == 2) {
			UTILS.changeQuestVisibility((Integer)arguments.get(0).getArgument(), (Boolean)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("changeCharacterZone") && arguments.size() == 2) {
			UTILS.changeCharacterZone((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("startTimer") && arguments.size() == 1) {
			UTILS.startTimer((Integer)arguments.get(0).getArgument());
		} else if (function.equalsIgnoreCase("stopTimer") && arguments.size() == 1) {
			UTILS.stopTimer((Integer)arguments.get(0).getArgument());
		} else if (function.equalsIgnoreCase("set") && arguments.size() == 1) {
			result = UTILS.set(arguments.get(0).getArgument());
		} else if (function.equalsIgnoreCase("stringtoint") && arguments.size() == 1) {
			result = UTILS.stringToInt((String)arguments.get(0).getArgument());
		} else if (function.equalsIgnoreCase("stringtodouble") && arguments.size() == 1) {
			result = UTILS.stringToDouble((String)arguments.get(0).getArgument());
		}
		
		if (result != null) {
			setTarget(result);
		}
	}
}
