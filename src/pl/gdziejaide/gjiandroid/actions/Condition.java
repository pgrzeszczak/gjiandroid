package pl.gdziejaide.gjiandroid.actions;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.ConditionOperatorType;

/**
 * Klasa warunku
 *
 * @author Przemek
 *
 */
public class Condition implements ConditionInterface {
	Argument leftArgument = null;
	Argument rightArgument = null;
	ConditionOperatorType operatorType = ConditionOperatorType.UNKNOWN;

	public void setLeftArgument(Argument leftArgument) {
		this.leftArgument = leftArgument;
	}

	public void setRightArgument(Argument rightArgument) {
		this.rightArgument = rightArgument;
	}

	public void setOperatorType(ConditionOperatorType operatorType) {
		this.operatorType = operatorType;
	}

	public boolean evaluate() {
		if (leftArgument == null || rightArgument == null || operatorType == ConditionOperatorType.UNKNOWN) {
			return false;
		}
		ArgumentType type = ArgumentType.UNKNOWN;
		if (leftArgument.getRealArgumentType() == ArgumentType.DOUBLE && rightArgument.getRealArgumentType() == ArgumentType.DOUBLE) {
			type = ArgumentType.DOUBLE;
//		} else if (leftArgument.getRealArgumentType() == ArgumentType.DOUBLE && rightArgument.getRealArgumentType() == ArgumentType.INTEGER) {
//			type = ArgumentType.DOUBLE;
//		} else if (leftArgument.getRealArgumentType() == ArgumentType.INTEGER && rightArgument.getRealArgumentType() == ArgumentType.DOUBLE) {
//			type = ArgumentType.DOUBLE;
		} else if (leftArgument.getRealArgumentType() == ArgumentType.INTEGER && rightArgument.getRealArgumentType() == ArgumentType.INTEGER) {
			type = ArgumentType.INTEGER;
		} else if (leftArgument.getRealArgumentType() == ArgumentType.BOOLEAN && rightArgument.getRealArgumentType() == ArgumentType.BOOLEAN) {
			type = ArgumentType.BOOLEAN;
		}
		switch (type) {
			case INTEGER:
				int leftI = ((Integer)leftArgument.getArgument()).intValue();
				int rightI = ((Integer)rightArgument.getArgument()).intValue();
				switch (operatorType) {
					case EQUAL: return leftI == rightI;
					case LESS: return leftI < rightI;
					case GREATER: return leftI > rightI;
					case NOT_EQUAL: return leftI != rightI;
					case GREATER_EQUAL: return leftI >= rightI;
					case LESS_EQUAL: return leftI <= rightI;
					default: break;
				}
				break;
			case DOUBLE:
				double leftD = ((Double)leftArgument.getArgument()).doubleValue();
				double rightD = ((Double)rightArgument.getArgument()).doubleValue();
				switch (operatorType) {
					case EQUAL: return leftD == rightD;
					case LESS: return leftD < rightD;
					case GREATER: return leftD > rightD;
					case NOT_EQUAL: return leftD != rightD;
					case GREATER_EQUAL: return leftD >= rightD;
					case LESS_EQUAL: return leftD <= rightD;
					default: break;
				}
				break;
			case BOOLEAN:
				boolean leftB = ((Boolean)leftArgument.getArgument()).booleanValue();
				boolean rightB = ((Boolean)rightArgument.getArgument()).booleanValue();
				switch (operatorType) {
					case EQUAL: return leftB == rightB;
					case NOT_EQUAL: return leftB != rightB;
					default: break;
				}
				break;
			default: break;
		}
		return false;
	}

}
