package pl.gdziejaide.gjiandroid.actions;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.PackageType;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;

public abstract class CallFunctionAction extends Action {
	protected PackageType packageType;
	protected String target;
	protected String function;
	protected List<Argument> arguments;

	public CallFunctionAction(PackageType packageType, String function, String target) {
		super();
		this.packageType = packageType;
		this.function = function;
		this.target = target;
		arguments = new ArrayList<Argument>();
	}
	
	public static CallFunctionAction createForPackageType(PackageType packageType, String function, String target) {
		switch (packageType) {
			case MATH: return new CallFunctionActionMath(function, target);
			case GUI: return new CallFunctionActionGui(function, target);
			case STRINGS: return new CallFunctionActionString(function, target);
			case UTILS: return new CallFunctionActionUtils(function, target);
			default: break;
		}
		return null;
	}
	
	public void addArguments(List<Argument> arguments) {
		this.arguments.addAll(arguments);
	}
	
	public void addArgument(Argument argument) {
		this.arguments.add(argument);
	}
	
	protected Object[] getArrayOfPreparedArguments() {
		List<Object> argsList = new ArrayList<Object>();
		for (Argument arg: arguments) {
			argsList.add(arg.getArgument());
		}
		return argsList.toArray();
	}
	
	protected ArgumentType getTypeOfFirstArgument() {
		if (arguments.size() > 0) {
			return arguments.get(0).getRealArgumentType();
		}
		return ArgumentType.UNKNOWN;
	}
	
	protected int[] toPrimitiveIntegerArray(Object[] integers) {
		int[] primitives = new int[integers.length];
		for (int i=0; i< integers.length; i++) {
			primitives[i] = (Integer)integers[i];
		}
		return primitives;
	}
	
	protected double[] toPrimitiveDoubleArray(Object[] doubles) {
		double[] primitives = new double[doubles.length];
		for (int i=0; i< doubles.length; i++) {
			primitives[i] = (Double)doubles[i];
		}
		return primitives;
	}
	
	protected String[] toPrimitiveStringArray(Object[] strings) {
		String[] primitives = new String[strings.length];
		for (int i=0; i< strings.length; i++) {
			primitives[i] = (String)strings[i];
		}
		return primitives;
	}
	
	protected void setTarget(Object result) {
		if (!target.equalsIgnoreCase("")) {
			String[] splitted = target.split("\\.");
			int id = Integer.parseInt(splitted[0]);
			String variable = "value";
			if (splitted.length >=2) {
				variable = splitted[1];
			}
			ObjectsContainer.getById(id).set(variable, result);
		}
	}
}
