package pl.gdziejaide.gjiandroid.actions;

import java.util.ArrayList;
import java.util.List;

import pl.gdziejaide.gjiandroid.enums.EventType;

public class EventAction extends Action {
	public EventType type;
	public List<Action> subActions;

	public EventAction(EventType type) {
		super();
		this.type = type;
		subActions = new ArrayList<Action>();
	}
	
	public EventType getType() {
		return type;
	}
	
	public void addSubAction(Action action) {
		subActions.add(action);
	}

	@Override
	public void process() {
		for (Action action: subActions) {
			action.process();
		}
	}
}
