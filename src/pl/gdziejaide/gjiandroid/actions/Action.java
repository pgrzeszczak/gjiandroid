package pl.gdziejaide.gjiandroid.actions;

public abstract class Action {
	public abstract void process();
}
