package pl.gdziejaide.gjiandroid.actions;

public interface ConditionInterface {
	public boolean evaluate();
}
