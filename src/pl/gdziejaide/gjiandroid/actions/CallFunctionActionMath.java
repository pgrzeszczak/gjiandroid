package pl.gdziejaide.gjiandroid.actions;

import pl.gdziejaide.gjiandroid.api.MATH;
import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.enums.PackageType;

public class CallFunctionActionMath extends CallFunctionAction {

	public CallFunctionActionMath(String function, String target) {
		super(PackageType.MATH, function, target);
	}

	public void process() {
		Object result = null;
		if (function.equalsIgnoreCase("sum") && arguments.size() > 0) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.sum(toPrimitiveIntegerArray(getArrayOfPreparedArguments()));
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.sum(toPrimitiveDoubleArray(getArrayOfPreparedArguments()));
			}
		} else if (function.equalsIgnoreCase("add") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.add((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.add((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("minus") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.minus((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.minus((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("div") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.div((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.div((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("multi") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.multi((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.multi((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("abs") && arguments.size() == 1) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.abs((Integer)arguments.get(0).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.abs((Double)arguments.get(0).getArgument());
			}
		} else if (function.equalsIgnoreCase("mod") && arguments.size() == 2) {
			result = MATH.mod((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("power") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.power((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.power((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("root") && arguments.size() == 2) {
			result = MATH.root((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("max") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.max((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.max((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("min") && arguments.size() == 2) {
			if (getTypeOfFirstArgument() == ArgumentType.INTEGER) {
				result = MATH.min((Integer)arguments.get(0).getArgument(), (Integer)arguments.get(1).getArgument());
			} else if (getTypeOfFirstArgument() == ArgumentType.DOUBLE) {
				result = MATH.min((Double)arguments.get(0).getArgument(), (Double)arguments.get(1).getArgument());
			}
		} else if (function.equalsIgnoreCase("round") && arguments.size() == 1) {
			result = MATH.round((Double)arguments.get(0).getArgument());
		}
		if (result != null) {
			setTarget(result);
		}
	}
}
