package pl.gdziejaide.gjiandroid.actions;

import java.util.ArrayList;
import java.util.List;

public class WhileLoopAction extends Action {
	ConditionInterface condition = null;
	List<Action> actions;

	public WhileLoopAction() {
		actions = new ArrayList<Action>();
	}
	
	public void addAction(Action action) {
		actions.add(action);
	}
	
	public void addActions(List<Action> actions) {
		this.actions.addAll(actions);
	}
	
	public void setCondition(ConditionInterface condition) {
		this.condition = condition;
	}

	@Override
	public void process() {
		if (condition != null) {
			while (condition.evaluate()) {
				for (Action action: actions) {
					action.process();
				}
			}
		}
	}

}
