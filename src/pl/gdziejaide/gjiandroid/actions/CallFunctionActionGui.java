package pl.gdziejaide.gjiandroid.actions;

import pl.gdziejaide.gjiandroid.api.GUI;
import pl.gdziejaide.gjiandroid.enums.PackageType;

public class CallFunctionActionGui extends CallFunctionAction {

	public CallFunctionActionGui(String function, String target) {
		super(PackageType.GUI, function, target);
	}

	public void process() {
		Object result = null;
		if (function.equalsIgnoreCase("message") && arguments.size() == 2) {
			GUI.message((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("warning") && arguments.size() == 2) {
			GUI.warning((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("choice") && arguments.size() > 2) {
			result = GUI.choice(toPrimitiveStringArray(getArrayOfPreparedArguments()));
		} else if (function.equalsIgnoreCase("dialog") && arguments.size() == 2) {
			result = GUI.dialog((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("confirm") && arguments.size() == 2) {
			result = GUI.confirm((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("showmedia") && arguments.size() == 1) {
			result = GUI.showMedia((Integer)arguments.get(0).getArgument());
		}
		if (result != null) {
			setTarget(result);
		}
	}
}
