package pl.gdziejaide.gjiandroid.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.gdziejaide.gjiandroid.enums.ArgumentType;
import pl.gdziejaide.gjiandroid.objects.ObjectsContainer;

public class Argument {
	ArgumentType type;
	String value;
	
	public Argument(ArgumentType type, String value) {
		super();
		this.type = type;
		this.value = value;
	}
	
	public Object getArgument() {
		switch (type) {
			case INTEGER: return Integer.parseInt(value);
			case BOOLEAN: return Boolean.parseBoolean(value);
			case CHAR: return value.charAt(0);
			case DOUBLE: return Double.parseDouble(value);
			case STRING: return preParseString(value);
			case VALUE: return parseValueReference();
			default: break;
		}
		return null;
	}
	
	public ArgumentType getRealArgumentType() {
		if (type == ArgumentType.VALUE) {
			return parseValueType();
		}
		return type;
	}
	
	protected Object parseValueReference() {
		String[] splitted = value.split("\\.");
		int id = Integer.parseInt(splitted[0]);
		String variable = "value";
		if (splitted.length >=2) {
			variable = splitted[1];
		}
		Object result = ObjectsContainer.getById(id).get(variable);
		if (parseValueType() == ArgumentType.STRING) {
			result = preParseString((String) result);
		}
		return result;
	}
	
	protected ArgumentType parseValueType() {
		String[] splitted = value.split("\\.");
		int id = Integer.parseInt(splitted[0]);
		String variable = "value";
		if (splitted.length >=2) {
			variable = splitted[1];
		}
		return ObjectsContainer.getById(id).getFieldType(variable);
	}
	
	protected String preParseString(String input) {
		StringBuffer resultString = new StringBuffer();
		Pattern regex = Pattern.compile("\\{\\{\\{(.*?)\\}\\}\\}");
		Matcher regexMatcher = regex.matcher(input);
		while (regexMatcher.find()) {
			try {
				String matched = regexMatcher.group(1);
				String[] splitted = matched.split("\\.");
				int id = Integer.parseInt(splitted[0]);
				String variable = "value";
				if (splitted.length >=2) {
					variable = splitted[1];
				}
				regexMatcher.appendReplacement(resultString, ObjectsContainer.getById(id).get(variable).toString());
			} catch (Exception e) {
				regexMatcher.appendReplacement(resultString, regexMatcher.group(0));
			}
		}
		regexMatcher.appendTail(resultString);
		return resultString.toString();
	}
}
