package pl.gdziejaide.gjiandroid.actions;

import java.util.ArrayList;
import java.util.List;

public class IfStatementAction extends Action {
	List<Action> ifActions;
	List<Action> elseActions;
	ConditionInterface condition = null;
	
	public IfStatementAction() {
		ifActions = new ArrayList<Action>();
		elseActions = new ArrayList<Action>();
	}
	
	public void addIfAction(Action action) {
		ifActions.add(action);
	}
	
	public void addIfActions(List<Action> actions) {
		ifActions.addAll(actions);
	}
	
	public void addElseAction(Action action) {
		elseActions.add(action);
	}
	
	public void addElseActions(List<Action> actions) {
		elseActions.addAll(actions);
	}
	
	public void setCondition(ConditionInterface condition) {
		this.condition = condition;
	}

	public void process() {
		if (condition != null) {
			if (condition.evaluate()) {
				for (Action action: ifActions) {
					action.process();
				}
			} else {
				for (Action action: elseActions) {
					action.process();
				}
			}
		}
	}

}
