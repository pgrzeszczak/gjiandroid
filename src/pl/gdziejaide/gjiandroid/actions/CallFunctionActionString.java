package pl.gdziejaide.gjiandroid.actions;

import pl.gdziejaide.gjiandroid.api.STRING;
import pl.gdziejaide.gjiandroid.enums.PackageType;

public class CallFunctionActionString extends CallFunctionAction {

	public CallFunctionActionString(String function, String target) {
		super(PackageType.STRINGS, function, target);
	}

	public void process() {
		Object result = null;
		if (function.equalsIgnoreCase("append") && arguments.size() == 2) {
			result = STRING.append((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("contains") && arguments.size() == 2) {
			result = STRING.contains((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		} else if (function.equalsIgnoreCase("remove") && arguments.size() == 2) {
			result = STRING.remove((String)arguments.get(0).getArgument(), (String)arguments.get(1).getArgument());
		}
		if (result != null) {
			setTarget(result);
		}
	}
}
