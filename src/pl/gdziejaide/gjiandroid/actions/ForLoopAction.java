package pl.gdziejaide.gjiandroid.actions;

import java.util.ArrayList;
import java.util.List;

public class ForLoopAction extends Action {
	int startValue;
	int endValue;
	int step;
	List<Action> actions;

	public ForLoopAction(int startValue, int endValue, int step) {
		super();
		this.startValue = startValue;
		this.endValue = endValue;
		this.step = step;
		actions = new ArrayList<Action>();
	}
	
	public void addAction(Action action) {
		actions.add(action);
	}
	
	public void addActions(List<Action> actions) {
		this.actions.addAll(actions);
	}

	@Override
	public void process() {
		for (int i=startValue; i<endValue; i+=step) {
			for(Action action: actions) {
				action.process();
			}
		}
	}

}
